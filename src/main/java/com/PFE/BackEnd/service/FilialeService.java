package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Filiale;
import com.PFE.BackEnd.repository.FilialeRepository;

@Service
public class FilialeService {

	
	private FilialeRepository filR;
	
	@Autowired
	public FilialeService(FilialeRepository filR) {
		this.filR=filR;
	}
	
	public void save(Filiale f) {
		filR.save(f);
	}
	
	public Iterable<Filiale> findAll() {
		return filR.findAll();
	}
	
	public void delete(Filiale c) {
		filR.delete(c);
	}
}
