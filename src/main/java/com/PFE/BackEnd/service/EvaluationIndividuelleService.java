package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.EvaluationIndividuelle;
import com.PFE.BackEnd.repository.EvaluationIndividuelleRepository;

@Service
public class EvaluationIndividuelleService {
	private EvaluationIndividuelleRepository AvIR;
	
	@Autowired
	public EvaluationIndividuelleService(EvaluationIndividuelleRepository AvIR) {
		this.AvIR=AvIR;
	}
	
	public void save(EvaluationIndividuelle p) {
		try {
			AvIR.save(p);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Iterable<EvaluationIndividuelle> findAll() {
		return AvIR.findAll();
	}
}
