package com.PFE.BackEnd.service;

import org.apache.commons.text.RandomStringGenerator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.User;
import com.PFE.BackEnd.Model.Role;
import com.PFE.BackEnd.repository.RoleRepository;
import com.PFE.BackEnd.repository.UserRepository;

@Service
public class UserService{
	
	
	private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
	
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findUserByUserName(String userName) {
        return userRepository.findByUsername(userName);
    }
    
	public void save(User u) {
		userRepository.save(u);
	}
	
	public Iterable<User> findAllUsers() {
		return userRepository.findAll();
	}
	
	public User findByUserName(String username) {
		return userRepository.findByUsername(username);
	}
	
	public Optional<User> findById(Long  id) {
		return userRepository.findById(id);
	}
	
	public void deleteUserById(Long  id) {
		userRepository.deleteById(id);
	}
	
	public boolean matches(String rawPassword, String encodedPassword) {
		return bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
	}
	
	public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByRole("ADMIN");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return userRepository.save(user);
    }
	
	public String generateRandomString() {
		RandomStringGenerator generator = new RandomStringGenerator.Builder()
			     .withinRange('0', 'z')
			     .filteredBy(Character::isLetterOrDigit)
			     .build();
		 String randomLetters = generator.generate(15);
		 return randomLetters;
	}
}
