package com.PFE.BackEnd.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.repository.EmployeRepository;

@Service
public class EmployeService {
	private EmployeRepository emR;
	
	
	@Autowired
	public EmployeService(EmployeRepository emR) {
		this.emR=emR;
	}
	
	public void save(Employe e) {
		emR.save(e);
	}
	
	public List<Employe> findAll() {
		return emR.findAll();
		}
	
	public Optional<Employe> findById(Integer id) {
		return emR.findById(id);
	}
	
	public List<Employe> findBySupperieur(Employe superieur) {
		return emR.findBySuperieur(superieur);
	}
	
	public ArrayList<Employe> findImpossibleBySupperieur(Employe superieur){
		ArrayList<Employe> listEmployes = (ArrayList<Employe>) this.findBySupperieur(superieur);
		ArrayList<Employe> newList = new ArrayList<Employe>();
		
		if (!listEmployes.isEmpty()) {
		
			for (Employe emp : listEmployes) {
				newList.addAll(this.findImpossibleBySupperieur(emp));
			}	
		}
		listEmployes.addAll(newList);
		listEmployes.add(superieur);
		return listEmployes;
	}
	
	public List<Employe> findPotentielSupperieur(Employe employe) {
		
		ArrayList<Employe> listEmp = (ArrayList<Employe>) emR.findAll();
		ArrayList<Employe> impossibleSuperieurs = new ArrayList<Employe>();
		impossibleSuperieurs.addAll(this.findImpossibleBySupperieur(employe));
		
		listEmp.removeAll(impossibleSuperieurs);
		return listEmp;		
		
	}
}
