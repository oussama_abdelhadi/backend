package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Poste;
import com.PFE.BackEnd.repository.PosteRepository;

@Service
public class PosteService {

private PosteRepository posteRepository;	
	
	@Autowired
	public PosteService(PosteRepository posteRepository) {
		this.posteRepository=posteRepository;
	}
	
	public void save(Poste o) {
		posteRepository.save(o);
	}
	
	public Iterable<Poste> findAll() {
		return posteRepository.findAll();
	}
}
