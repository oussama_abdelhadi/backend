package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Objectif;
import com.PFE.BackEnd.repository.ObjectifRepository;

@Service
public class ObjectifService {

private ObjectifRepository objR;
	
	
	@Autowired
	public ObjectifService(ObjectifRepository objR) {
		this.objR=objR;
	}
	
	public void save(Objectif o) {
		objR.save(o);
	}
	
	public Iterable<Objectif> findAll() {
		return objR.findByObjectifType();
	}
}
