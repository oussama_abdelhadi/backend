package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.PFE.BackEnd.Model.Notification;
import com.PFE.BackEnd.repository.NotificationRepository;

@Service
public class NotificationService {
	
	private NotificationRepository notifR;
	
	@Autowired
	public NotificationService(NotificationRepository notifR) {
		this.notifR=notifR;
	}
	
	public void save(Notification e) {
		try {
			notifR.save(e);
		}catch(Exception te) {
			te.printStackTrace();
		}
		
	}
	
	public Iterable<Notification> findAll() {
		return notifR.findAll();
	}
	
	public void delete(Notification e) {
		notifR.delete(e);
	}
}