package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Regle;
import com.PFE.BackEnd.repository.RegleRepository;

@Service
public class RegleService {

	private RegleRepository decR;
	
	@Autowired
	public RegleService(RegleRepository decR) {
		this.decR=decR;
	}
	
	public void save(Regle e) {
		try {
			decR.save(e);
		}catch(Exception te) {
			te.printStackTrace();
		}
		
	}
	
	public Iterable<Regle> findAll() {
		return decR.findAll();
	}
	
	public void delete(Regle e) {
		decR.delete(e);
	}
}
