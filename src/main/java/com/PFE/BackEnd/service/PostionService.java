package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Position;
import com.PFE.BackEnd.repository.PositionRepository;

@Service
public class PostionService{

private PositionRepository repo;
	
	@Autowired
	public PostionService(PositionRepository repo) {
		this.repo=repo;
	}
	
	public void save(Position c) {
		repo.save(c);
	}
	
	public Iterable<Position> findAll() {
		return repo.findAll();
	}
	
	public void delete(Position c) {
		repo.delete(c);
	}
}
