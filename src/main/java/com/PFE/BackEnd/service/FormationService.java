package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Formation;
import com.PFE.BackEnd.repository.FormationRepository;

@Service
public class FormationService {
	
	private FormationRepository decR;
	
	@Autowired
	public FormationService(FormationRepository decR) {
		this.decR=decR;
	}
	
	public void save(Formation e) {
		try {
			decR.save(e);
		}catch(Exception te) {
			te.printStackTrace();
		}
		
	}
	
	public Iterable<Formation> findAll() {
		return decR.findAll();
	}
	
	public void delete(Formation e) {
		decR.delete(e);
	}
}
