package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Division;
import com.PFE.BackEnd.repository.DivisionRepository;

@Service
public class DivisionService {

	private DivisionRepository divR;
	
	@Autowired
	public DivisionService(DivisionRepository divR) {
		this.divR=divR;
	}
	
	public void save(Division c) {
		divR.save(c);
	}
	
	public Iterable<Division> findAll() {
		return divR.findAll();
	}
	
	public void delete(Division c) {
		divR.delete(c);
	}
}
