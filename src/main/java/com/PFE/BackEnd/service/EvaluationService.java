package com.PFE.BackEnd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Evaluation;
import com.PFE.BackEnd.Model.EvaluationIndividuelle;
import com.PFE.BackEnd.repository.EvaluationIndividuelleRepository;
import com.PFE.BackEnd.repository.EvaluationRepository;

@Service
public class EvaluationService {
	
	private EvaluationRepository evaR;
	private EvaluationIndividuelleRepository evaInR;
	
	@Autowired
	public EvaluationService(EvaluationRepository evaR, EvaluationIndividuelleRepository evaInR) {
		super();
		this.evaR = evaR;
		this.evaInR = evaInR;
	}
	
	public void saveE(Evaluation e) {
		evaR.save(e);
	}
	
	public List<Evaluation> findAllE() {
		return evaR.findByTypeEvaluation(true);
	}
	
	public Iterable<EvaluationIndividuelle> getAllEmployeEvaluations(Employe emp) {
		return evaInR.findByEmploye(emp);
	}
	
	public void saveEI(EvaluationIndividuelle e) {
		evaInR.save(e);
	}
	
	public Iterable<EvaluationIndividuelle> findAllEI() {
		return evaInR.findAll();
	}
}
