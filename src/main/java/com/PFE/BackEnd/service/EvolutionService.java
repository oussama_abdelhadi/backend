package com.PFE.BackEnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PFE.BackEnd.Model.Evolution;
import com.PFE.BackEnd.repository.EvolutionRepository;

@Service
public class EvolutionService {

	private EvolutionRepository decR;
	
	@Autowired
	public EvolutionService(EvolutionRepository decR) {
		this.decR=decR;
	}
	
	public void save(Evolution e) {
		try {
			decR.save(e);
		}catch(Exception te) {
			te.printStackTrace();
		}
		
	}
	
	public Iterable<Evolution> findAll() {
		return decR.findAll();
	}
	
	public void delete(Evolution e) {
		decR.delete(e);
	}
}
