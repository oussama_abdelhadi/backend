package com.PFE.BackEnd.Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.EXISTING_PROPERTY ,property = "type" ,visible = true)

/*@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")*/

@JsonSubTypes({
    @Type(value = Division.class, name = "division"),
    @Type(value = Filiale.class, name = "filiale"),
    @Type(value = Direction.class, name = "direction"),
    @Type(value = Service.class, name = "service")}
)

public abstract class Position {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codePosition;
	
	private String intitulePosition;
	
	private String type;


	  public String getType() {
	    return type;
	  }

	  public void setType(String type) {
	    this.type = type;
	  }
	
	public Position() {
		// TODO Auto-generated constructor stub
	}
	
	public Position(Integer codePosition, String intitulePosition) {
		this.codePosition = codePosition;
		this.intitulePosition = intitulePosition;
	}

	public Integer getCodePosition() {
		return codePosition;
	}

	public void setCodePosition(Integer codePosition) {
		this.codePosition = codePosition;
	}

	public String getIntitulePosition() {
		return intitulePosition;
	}

	public void setIntitulePosition(String intitulePosition) {
		this.intitulePosition = intitulePosition;
	}
	
	
	
	
}
