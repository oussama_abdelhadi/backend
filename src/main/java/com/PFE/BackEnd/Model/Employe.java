package com.PFE.BackEnd.Model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
public class Employe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeEmploye;
	private String nom;
	private String prenom;
	
	@ManyToOne(targetEntity = Poste.class, optional = true)
    private Poste poste;
	
	@ManyToOne(targetEntity = Employe.class, optional = true)
    private Employe superieur;
	
	/*@OneToMany(mappedBy="superieur", fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Employe> collaborateurs;*/
	/*
	@OneToMany(mappedBy="employe", fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<EvaluationIndividuelle> evaluations;
	*/
	/*@ManyToMany
	@JoinTable(
	  name = "suit_table", 
	  joinColumns = @JoinColumn(name = "codeEmploye"), 
	  inverseJoinColumns = @JoinColumn(name = "codeFormation"))
	private List<Formation> formations;*/
	
	/*@ManyToMany
	@JoinTable(
	  name = "niv_compt_table", 
	  joinColumns = @JoinColumn(name = "codeEmploye"), 
	  inverseJoinColumns = @JoinColumn(name = "codeNiveau"))
	private List<Niveau> niveaux;
	*/
	
	@OneToOne(targetEntity = User.class, orphanRemoval=true)
	@NotFound(action = NotFoundAction.IGNORE)
	private User user;

	public Employe() {
		// TODO Auto-generated constructor stub
	}
	
	public Employe(String nom, String prenom, Poste poste, Employe superieur, User user) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.poste = poste;
		this.superieur = superieur;
		this.user = user;
	}

	public Employe(Integer codeEmploye,String nom, String prenom, User user) {
		this.codeEmploye=codeEmploye;
		this.nom=nom;
		this.prenom=prenom;
		this.user = user;
	}
	
	public Integer getCodeEmploye() {
		return codeEmploye;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}

	public void setCodeEmploye(Integer codeEmploye) {
		this.codeEmploye = codeEmploye;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Employe getSuperieur() {
		return superieur;
	}

	public void setSuperieur(Employe superieur) {
		this.superieur = superieur;
	}

	public Poste getPoste() {
		return poste;
	}

	public void setPoste(Poste poste) {
		this.poste = poste;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	@Override
    public boolean equals(Object anObject) {
        if (!(anObject instanceof Employe)) {
            return false;
        }
        Employe otherEmploye = (Employe)anObject;
        return otherEmploye.getCodeEmploye().equals(this.getCodeEmploye());
    }

}
