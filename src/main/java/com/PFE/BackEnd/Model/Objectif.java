package com.PFE.BackEnd.Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Objectif {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeObjectif;
	private String nomObjectif;
	private boolean typeObjectif;
	
	@ManyToOne(targetEntity = Position.class, optional = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Position position;
	
	public Objectif(Integer codeObjectif, String nomObjectif,Position position,boolean typeObjectif) {
		super();
		this.codeObjectif = codeObjectif;
		this.nomObjectif = nomObjectif;
		this.position = position;
		this.typeObjectif = typeObjectif;
	}
	
	public Objectif(Integer codeObjectif, String nomObjectif) {
		super();
		this.codeObjectif = codeObjectif;
		this.nomObjectif = nomObjectif;
	}



	public Objectif() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCodeObjectif() {
		return codeObjectif;
	}

	public void setCodeObjectif(Integer codeObjectif) {
		this.codeObjectif = codeObjectif;
	}

	public String getNomObjectif() {
		return nomObjectif;
	}

	public void setNomObjectif(String nomObjectif) {
		this.nomObjectif = nomObjectif;
	}



	public boolean getTypeObjectif() {
		return typeObjectif;
	}



	public void setTypeObjectif(boolean typeObjectif) {
		this.typeObjectif = typeObjectif;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
	
	
}
