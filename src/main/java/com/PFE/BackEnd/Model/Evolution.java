package com.PFE.BackEnd.Model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Evolution {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeEvolution;
	private Integer date;
	private Integer type;
	private String posteSouhaite;
	private String preferenceGeo;
	private Date echeanceEvolution;
	private String commentaireCol;
	private String avisResp;
	private Integer voteResp;
	
	
	@ManyToOne(targetEntity = Employe.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Employe employe;
	
	
	
	public Evolution(Integer codeEvolution, Integer date, Integer type, String posteSouhaite, String preferenceGeo,
			Date echeanceEvolution, String commentaireCol, String avisResp, Integer voteResp, Employe employe) {
		super();
		this.codeEvolution = codeEvolution;
		this.date = date;
		this.type = type;
		this.posteSouhaite = posteSouhaite;
		this.preferenceGeo = preferenceGeo;
		this.echeanceEvolution = echeanceEvolution;
		this.commentaireCol = commentaireCol;
		this.avisResp = avisResp;
		this.voteResp = voteResp;
		this.employe = employe;
	}



	public Evolution() {
		// TODO Auto-generated constructor stub
	}



	public Integer getCodeEvolution() {
		return codeEvolution;
	}



	public void setCodeEvolution(Integer codeEvolution) {
		this.codeEvolution = codeEvolution;
	}



	public Integer getDate() {
		return date;
	}



	public void setDate(Integer date) {
		this.date = date;
	}



	public Integer getType() {
		return type;
	}



	public void setType(Integer type) {
		this.type = type;
	}



	public String getPosteSouhaite() {
		return posteSouhaite;
	}



	public void setPosteSouhaite(String posteSouhaite) {
		this.posteSouhaite = posteSouhaite;
	}



	public String getPreferenceGeo() {
		return preferenceGeo;
	}



	public void setPreferenceGeo(String preferenceGeo) {
		this.preferenceGeo = preferenceGeo;
	}



	public Date getEcheanceEvolution() {
		return echeanceEvolution;
	}



	public void setEcheanceEvolution(Date echeanceEvolution) {
		this.echeanceEvolution = echeanceEvolution;
	}



	public String getCommentaireCol() {
		return commentaireCol;
	}



	public void setCommentaireCol(String commentaireCol) {
		this.commentaireCol = commentaireCol;
	}



	public String getAvisResp() {
		return avisResp;
	}



	public void setAvisResp(String avisResp) {
		this.avisResp = avisResp;
	}



	public Employe getEmploye() {
		return employe;
	}



	public void setEmploye(Employe employe) {
		this.employe = employe;
	}



	public Integer getVoteResp() {
		return voteResp;
	}



	public void setVoteResp(Integer voteResp) {
		this.voteResp = voteResp;
	}
	
	
	
	
}
