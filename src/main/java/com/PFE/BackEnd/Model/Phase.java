package com.PFE.BackEnd.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Phase {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonProperty(access = Access.WRITE_ONLY)
	private Integer id;
	
	private Integer date;
	private Integer etape;
	
	public Phase(Integer id, Integer date, Integer etape) {
		super();
		this.id = id;
		this.date = date;
		this.etape = etape;
	}
	
	
	
	public Phase(Integer date, Integer etape) {
		super();
		this.date = date;
		this.etape = etape;
	}



	public Phase() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}



	public Integer getEtape() {
		return etape;
	}



	public void setEtape(Integer etape) {
		this.etape = etape;
	}

	
	
	
}
