package com.PFE.BackEnd.Model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;


@Entity
@Table(name = "user") 
public class User {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String email;
	private String username;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	
	/*@OneToOne(optional = true)
	private Employe emp;*/
	
	@ManyToMany
    @JoinTable(name = "user_role", 
    joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
	
	public User(String userName, String password) {
		this.username = userName;
		this.password = password;
	}
	
	
	public User(String username, String password, Set<Role> roles) {
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public User(Long id, String email, String username, String password) {
		super();
		this.id = id;
		this.email = email;
		this.username = username;
		this.password = password;
	}
	
	public User(String username,String email, String password) {
		super();
		this.email = email;
		this.username = username;
		this.password = password;
	}
	
	
/*	
	public User(String email, String userName, String password, Set<Role> roles) {
		super();
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.roles = roles;
	}

*/
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public void initRoles() {
		this.roles =  new HashSet<>();
	}
	public Long getId() {
		return id;
	}
	public String getPassword() {
		return password;
	}
	public String getUsername() {
		return username;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setUserName(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return username + email + roles;
	}
}
