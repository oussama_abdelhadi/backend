package com.PFE.BackEnd.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Notification {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer idNotif;
	private String message;
	
	@ManyToOne(targetEntity = User.class,optional = false)
    private User user;

	public Notification(Integer idNotif, String message, User user) {
		super();
		this.idNotif = idNotif;
		this.message = message;
		this.user = user;
	}

	public Notification() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getIdNotif() {
		return idNotif;
	}

	public void setIdNotif(Integer idNotif) {
		this.idNotif = idNotif;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
