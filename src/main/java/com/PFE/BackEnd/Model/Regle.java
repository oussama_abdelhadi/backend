package com.PFE.BackEnd.Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Regle {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeRegle;
	private String nomRegle;
	private Integer conditionRegle;
	private Integer actionRegle;
	
	

	public Regle(Integer codeRegle, String nomRegle, Integer conditionRegle, Integer actionRegle) {
		super();
		this.codeRegle = codeRegle;
		this.nomRegle = nomRegle;
		this.conditionRegle =conditionRegle;
		this.actionRegle = actionRegle;
	}
	
	public Regle() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCodeRegle() {
		return codeRegle;
	}

	public void setCodeRegle(Integer codeRegle) {
		this.codeRegle = codeRegle;
	}

	public String getNomRegle() {
		return nomRegle;
	}

	public void setNomRegle(String nomRegle) {
		this.nomRegle = nomRegle;
	}

	

	public Integer getConditionRegle() {
		return conditionRegle;
	}

	public void setConditionRegle(Integer conditionRegle) {
		this.conditionRegle = conditionRegle;
	}

	public Integer getActionRegle() {
		return actionRegle;
	}

	public void setActionRegle(Integer actionRegle) {
		this.actionRegle = actionRegle;
	}

	

	
}
