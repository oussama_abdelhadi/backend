package com.PFE.BackEnd.Model;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@JsonTypeName("filiale")
public class Filiale extends Position{
	
	@NotBlank
	private String codeFiliale;
	
	@ManyToOne(targetEntity = Division.class, optional = true,cascade = CascadeType.MERGE)
	private Division division;
	
	
	public Filiale(Integer codePosition, String codeFiliale, String intituleFiliale, Division division) {
		super(codePosition,intituleFiliale);
		this.division = division;
		this.codeFiliale = codeFiliale;
	}
	
	
	
	public Filiale(Integer codePosition, String codeFiliale, String intituleFiliale) {
		super(codePosition,intituleFiliale);
		this.codeFiliale = codeFiliale;
	}
	public String getCodeFiliale() {
		return codeFiliale;
	}



	public void setCodeFiliale(String codeFiliale) {
		this.codeFiliale = codeFiliale;
	}



	public Filiale() {
		super();
	}
	
	public Division getDivision() {
		return division;
	}
	public void setDivision(Division division) {
		this.division = division;
	}


	
	
}
