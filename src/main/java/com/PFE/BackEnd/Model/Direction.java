package com.PFE.BackEnd.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@JsonTypeName("direction")
public class Direction extends Position{

	@NotBlank
	private String codeDirection;
	
	
	@ManyToOne(targetEntity = Filiale.class, optional = true, cascade = CascadeType.MERGE)
	private Filiale filiale;
	
	public Direction(int codePosition, String codeDirection, String intituleDirection, Filiale filiale) {
		super(codePosition, intituleDirection);
		this.filiale = filiale;
		this.codeDirection = codeDirection;
	}

	
	public Direction(int codePosition, String codeDirection, String intituleDirection) {
		super(codePosition, intituleDirection);
		this.codeDirection = codeDirection;
	}

	public Direction() {
		// TODO Auto-generated constructor stub
	}
	

	
	public Filiale getFiliale() {
		return filiale;
	}

	public void setFiliale(Filiale filiale) {
		this.filiale = filiale;
	}


	public String getCodeDirection() {
		return codeDirection;
	}


	public void setCodeDirection(String codeDirection) {
		this.codeDirection = codeDirection;
	}
	
	
	
	
}
