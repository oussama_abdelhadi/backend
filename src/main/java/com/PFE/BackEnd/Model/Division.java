package com.PFE.BackEnd.Model;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@JsonTypeName("division")
public class Division extends Position{

	@NotBlank
	private String codeDivision;
	
	public Division() {
		super();
	}
	
	public Division(Integer codePosition, String codeDivision, String intitulePosition) {
		super(codePosition,intitulePosition);
		this.codeDivision = codeDivision;
	}

	public String getCodeDivision() {
		return codeDivision;
	}

	public void setCodeDivision(String codeDivision) {
		this.codeDivision = codeDivision;
	}


}
