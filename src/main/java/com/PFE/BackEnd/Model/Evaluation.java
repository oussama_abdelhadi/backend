package com.PFE.BackEnd.Model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Evaluation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeEvaluation;
	private Double ponderation;
	private String planAction;
	private String KPI;
	private String cible;
	private Boolean typeEvaluation;
	private Double evalMiParcours;
	private Double evalFinale;
	private Integer annee;


	@ManyToOne(targetEntity = Objectif.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Objectif objectif;
	
	

	public Evaluation(Integer codeEvaluation, Double ponderation, String planAction, String kPI, String cible,
			Boolean typeEvaluation, Double evalMiParcours, Double evalFinale, Integer annee, Objectif objectif) {
		super();
		this.codeEvaluation = codeEvaluation;
		this.ponderation = ponderation;
		this.planAction = planAction;
		KPI = kPI;
		this.cible = cible;
		this.typeEvaluation = typeEvaluation;
		this.evalMiParcours = evalMiParcours;
		this.evalFinale = evalFinale;
		this.annee = annee;
		this.objectif = objectif;
	}
	public Evaluation(Integer codeEvaluation, Double ponderation, String planAction, String kPI,String cible, boolean typeEvaluation,
			Double evalMiParcours, Double evalFinale,
			Objectif objectif) {
		this.codeEvaluation = codeEvaluation;
		this.ponderation = ponderation;
		this.planAction = planAction;
		this.KPI = kPI;
		this.cible = cible;
		this.typeEvaluation = typeEvaluation;
		this.evalMiParcours = evalMiParcours;
		this.evalFinale = evalFinale;
		this.objectif = objectif;
	}
/*
	public Evaluation(Integer codeEvaluation, Double ponderation, String planAction, String kPI,
			Double echeancierDeRealisation, Double evalMiParcours, Double evalFinale, Objectif objectif) {
		super();
		this.codeEvaluation = codeEvaluation;
		this.ponderation = ponderation;
		this.planAction = planAction;
		KPI = kPI;
		this.echeancierDeRealisation = echeancierDeRealisation;
		this.evalMiParcours = evalMiParcours;
		this.evalFinale = evalFinale;
		this.objectif = objectif;
	}
	*/
	public Evaluation(Integer codeEvaluation) {
		this.codeEvaluation = codeEvaluation;

	}

	public Evaluation() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getCodeEvaluation() {
		return codeEvaluation;
	}

	public void setCodeEvaluation(Integer codeEvaluation) {
		this.codeEvaluation = codeEvaluation;
	}

	public Double getPonderation() {
		return ponderation;
	}

	public void setPonderation(Double ponderation) {
		this.ponderation = ponderation;
	}

	public String getPlanAction() {
		return planAction;
	}

	public void setPlanAction(String planAction) {
		this.planAction = planAction;
	}

	public String getKPI() {
		return KPI;
	}

	public void setKPI(String kPI) {
		KPI = kPI;
	}


	public Double getEvalMiParcours() {
		return evalMiParcours;
	}

	public void setEvalMiParcours(Double evalMiParcours) {
		this.evalMiParcours = evalMiParcours;
	}

	public Double getEvalFinale() {
		return evalFinale;
	}

	public void setEvalFinale(Double evalFinale) {
		this.evalFinale = evalFinale;
	}

	public Objectif getObjectif() {
		return objectif;
	}

	public void setObjectif(Objectif objectif) {
		this.objectif = objectif;
	}
	public Boolean getTypeEvaluation() {
		return typeEvaluation;
	}
	public void setTypeEvaluation(boolean typeEvaluation) {
		this.typeEvaluation = typeEvaluation;
	}
	public String getCible() {
		return cible;
	}
	public void setCible(String cible) {
		this.cible = cible;
	}
	public void setTypeEvaluation(Boolean typeEvaluation) {
		this.typeEvaluation = typeEvaluation;
	}
	public Integer getAnnee() {
		return annee;
	}
	public void setAnnee(Integer annee) {
		this.annee = annee;
	}
	
	
	
}
