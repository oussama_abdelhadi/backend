package com.PFE.BackEnd.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Ponderation {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private Double ponderation;
	
	private String planAction;
	
	@ManyToOne(targetEntity = Employe.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Employe employe;
	
	@ManyToOne(targetEntity = Evaluation.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Evaluation evaluation;

	
	public Ponderation(Integer id, Double ponderation, String planAction, Employe employe, Evaluation evaluation) {
		super();
		this.id = id;
		this.ponderation = ponderation;
		this.planAction = planAction;
		this.employe = employe;
		this.evaluation = evaluation;
	}

	
	public Ponderation() {
		// TODO Auto-generated constructor stub
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Double getPonderation() {
		return ponderation;
	}


	public void setPonderation(Double ponderation) {
		this.ponderation = ponderation;
	}


	public Employe getEmploye() {
		return employe;
	}


	public void setEmploye(Employe employe) {
		this.employe = employe;
	}


	public Evaluation getEvaluation() {
		return evaluation;
	}


	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}


	public String getPlanAction() {
		return planAction;
	}


	public void setPlanAction(String planAction) {
		this.planAction = planAction;
	}
	
	
}
