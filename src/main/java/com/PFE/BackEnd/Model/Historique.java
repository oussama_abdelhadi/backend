package com.PFE.BackEnd.Model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Historique {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeH;
	private String traitement;
	private Date dateH;
	
	@ManyToOne(targetEntity = User.class,optional = false)
    private User user;

	public Historique(Integer codeH, String traitement, Date dateH, User user) {
		super();
		this.codeH = codeH;
		this.traitement = traitement;
		this.dateH = dateH;
		this.user = user;
	}
	
	public Historique(String traitement, Date dateH, User user) {
		super();
		this.traitement = traitement;
		this.dateH = dateH;
		this.user = user;
	}

	public Historique() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCodeH() {
		return codeH;
	}

	public void setCodeH(Integer codeH) {
		this.codeH = codeH;
	}

	public String getTraitement() {
		return traitement;
	}

	public void setTraitement(String traitement) {
		this.traitement = traitement;
	}

	public Date getDateH() {
		return dateH;
	}

	public void setDateH(Date dateH) {
		this.dateH = dateH;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
