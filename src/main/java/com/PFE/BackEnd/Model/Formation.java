package com.PFE.BackEnd.Model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Formation {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeFormation;
	private String nomFormation;
	private String objectifPrevu;
	private Integer evalColl;
	private Integer evalResp;
	private Integer dateFormation;
	private String justification;
	private Boolean demande;
	
	@ManyToOne(targetEntity = Employe.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Employe employe;
	
	
	
	
	public Formation(Integer codeFormation, String nomFormation, String objectifPrevu, Integer evalColl,
			Integer evalResp, Integer dateFormation, String justification, Boolean demande, Employe employe) {
		super();
		this.codeFormation = codeFormation;
		this.nomFormation = nomFormation;
		this.objectifPrevu = objectifPrevu;
		this.evalColl = evalColl;
		this.evalResp = evalResp;
		this.dateFormation = dateFormation;
		this.justification = justification;
		this.demande = demande;
		this.employe = employe;
	}

	public Formation(Integer codeFormation, String nomFormation, String objectifPrevu) {
		super();
		this.codeFormation = codeFormation;
		this.nomFormation = nomFormation;
		this.objectifPrevu = objectifPrevu;
	}
	
	public Formation() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getCodeFormation() {
		return codeFormation;
	}
	public void setCodeFormation(Integer codeFormation) {
		this.codeFormation = codeFormation;
	}
	public String getNomFormation() {
		return nomFormation;
	}
	public void setNomFormation(String nomFormation) {
		this.nomFormation = nomFormation;
	}
	public String getObjectifPrevu() {
		return objectifPrevu;
	}
	public void setObjectifPrevu(String objectifPrevu) {
		this.objectifPrevu = objectifPrevu;
	}

	

	public Integer getEvalColl() {
		return evalColl;
	}

	public void setEvalColl(Integer evalColl) {
		this.evalColl = evalColl;
	}

	public Integer getEvalResp() {
		return evalResp;
	}

	public void setEvalResp(Integer evalResp) {
		this.evalResp = evalResp;
	}

	public Integer getDateFormation() {
		return dateFormation;
	}

	public void setDateFormation(Integer dateFormation) {
		this.dateFormation = dateFormation;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public Boolean getDemande() {
		return demande;
	}

	public void setDemande(Boolean demande) {
		this.demande = demande;
	}
	
	
}
