package com.PFE.BackEnd.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@JsonTypeName("service")
public class Service extends Position{
	
	@NotBlank
	private String codeService;
	
	@ManyToOne(targetEntity = Direction.class, optional = true, cascade = CascadeType.MERGE)
	private Direction direction;
	
	
	public Service(int codePosition, String codeService, String intituleService, Direction direction) {
		super(codePosition,intituleService);
		this.codeService = codeService;
		this.direction = direction;
	}
	
	public Service(int codePosition, String codeService, String intituleService) {
		super(codePosition,intituleService);
		this.codeService = codeService;
	}
	
	public Service() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public String getCodeService() {
		return codeService;
	}

	public void setCodeService(String codeService) {
		this.codeService = codeService;
	}
	
	
}
