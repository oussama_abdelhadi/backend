package com.PFE.BackEnd.Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Poste {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codePoste;
	private String intitulePoste;
	private String categoriePoste;
	
	@ManyToOne(targetEntity = Position.class, optional = true)
	private Position position;
	
	

	public Poste(Integer codePoste, String intitulePoste, String categoriePoste, Position position) {
		super();
		this.codePoste = codePoste;
		this.intitulePoste = intitulePoste;
		this.categoriePoste = categoriePoste;
		this.position = position;
	}

	public Poste(Integer codePoste, String intitulePoste, Position position) {
		this.codePoste = codePoste;
		this.intitulePoste = intitulePoste;
		this.position = position;
	}
	
	public Poste() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Integer getCodePoste() {
		return codePoste;
	}
	public void setCodePoste(Integer codePoste) {
		this.codePoste = codePoste;
	}
	public String getIntitulePoste() {
		return intitulePoste;
	}
	public void setIntitulePoste(String intitulePoste) {
		this.intitulePoste = intitulePoste;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}

	public String getCategoriePoste() {
		return categoriePoste;
	}

	public void setCategoriePoste(String categoriePoste) {
		this.categoriePoste = categoriePoste;
	}

	
	
}
