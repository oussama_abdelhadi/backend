package com.PFE.BackEnd.Model;


import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Validation {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer codeValidation;
	private Integer anneeVal;
	private Integer typeVal;
	private Boolean valCol;
	private Boolean valResp;
	private Date dateCol;
	private Date dateResp;
	private String commentaireCol;
	private String commentaireResp;
	private String pointFrot;
	private String pointAm;
	private Integer tro;
	
	@ManyToOne(targetEntity = Employe.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Employe employe;

	
	

	public Validation(Integer codeValidation, Integer anneeVal, Integer typeVal, Boolean valCol,
			Boolean valResp, Date dateCol, Date dateResp, String commentaireCol,
			String commentaireResp, String pointFrot, String pointAm, Integer tro, Employe employe) {
		super();
		this.codeValidation = codeValidation;
		this.anneeVal = anneeVal;
		this.typeVal = typeVal;
		this.valCol = valCol;
		this.valResp = valResp;
		this.dateCol = dateCol;
		this.dateResp = dateResp;
		this.commentaireCol = commentaireCol;
		this.commentaireResp = commentaireResp;
		this.pointFrot = pointFrot;
		this.pointAm = pointAm;
		this.tro = tro;
		this.employe = employe;
	}

	public Validation() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCodeValidation() {
		return codeValidation;
	}

	public void setCodeValidation(Integer codeValidation) {
		this.codeValidation = codeValidation;
	}

	public Integer getAnneeVal() {
		return anneeVal;
	}

	public void setAnneeVal(Integer anneeVal) {
		this.anneeVal = anneeVal;
	}

	public Integer getTypeVal() {
		return typeVal;
	}

	public void setTypeVal(Integer typeVal) {
		this.typeVal = typeVal;
	}

	public Boolean getValCol() {
		return valCol;
	}

	public void setValCol(Boolean valCol) {
		this.valCol = valCol;
	}

	

	public Boolean getValResp() {
		return valResp;
	}

	public void setValResp(Boolean valResp) {
		this.valResp = valResp;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public Date getDateCol() {
		return dateCol;
	}

	public void setDateCol(Date dateCol) {
		this.dateCol = dateCol;
	}

	public Date getDateResp() {
		return dateResp;
	}

	public void setDateResp(Date dateResp) {
		this.dateResp = dateResp;
	}

	public String getCommentaireCol() {
		return commentaireCol;
	}

	public void setCommentaireCol(String commentaireCol) {
		this.commentaireCol = commentaireCol;
	}

	public String getCommentaireResp() {
		return commentaireResp;
	}

	public void setCommentaireResp(String commentaireResp) {
		this.commentaireResp = commentaireResp;
	}

	public String getPointFrot() {
		return pointFrot;
	}

	public void setPointFrot(String pointFrot) {
		this.pointFrot = pointFrot;
	}

	public String getPointAm() {
		return pointAm;
	}

	public void setPointAm(String pointAm) {
		this.pointAm = pointAm;
	}

	public Integer getTro() {
		return tro;
	}

	public void setTro(Integer tro) {
		this.tro = tro;
	}
	
	
}
