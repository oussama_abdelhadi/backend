package com.PFE.BackEnd.Model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class EvaluationIndividuelle extends Evaluation{
	
	private static final long serialVersionUID = 1L;
	private Date echeancier;
	private Double evalMiParCollab;
	private Double evalFinCollab;
	private Double tauxAtteinte;
	private String commentaireCollabMiPar;
	private String commentaireRespMiPar;
	private String commentaireCollabFin;
	private String commentaireRespFin;
	
	
	@ManyToOne(targetEntity = Employe.class,optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Employe employe;
	
	/*@ManyToMany
	@JoinTable(
	  name = "regle_evalindv_table", 
	  joinColumns = @JoinColumn(name = "codeEvaluation"), 
	  inverseJoinColumns = @JoinColumn(name = "codeRegle"))
	private List<Regle> regles;*/
	
	
	
	public EvaluationIndividuelle(Integer codeEvaluation, Double ponderation, String planAction, String kPI, String cible, boolean typeEvaluation ,
			Date echeancier, Double evalMiParcours, Double evalFinale, Objectif objectif, Integer annee,
			Employe employe, Double evalMiParCollab, Double evalFinCollab , Double tauxAtteinte, String commentaireCollabMiPar , 
			String commentaireRespMiPar, String commentaireCollabFin, String commentaireRespFin) {
		super(codeEvaluation, ponderation, planAction, kPI, cible, typeEvaluation, evalMiParcours , evalFinale, annee, objectif);
		this.echeancier = echeancier;
		this.evalMiParCollab = evalMiParCollab;
		this.evalFinCollab = evalFinCollab;
		this.tauxAtteinte = tauxAtteinte;
		this.employe = employe;
		this.commentaireCollabMiPar = commentaireCollabMiPar;
		this.commentaireRespMiPar = commentaireRespMiPar;
		this.commentaireCollabFin = commentaireCollabFin;
		this.commentaireRespFin = commentaireRespFin;
		
	}
	public EvaluationIndividuelle() {
		// TODO Auto-generated constructor stub
	}

	public Double getEvalMiParCollab() {
		return evalMiParCollab;
	}

	public void setEvalMiParCollab(Double evalMiParCollab) {
		this.evalMiParCollab = evalMiParCollab;
	}

	public Double getEvalFinCollab() {
		return evalFinCollab;
	}

	public void setEvalFinCollab(Double evalFinCollab) {
		this.evalFinCollab = evalFinCollab;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	

	public Date getEcheancier() {
		return echeancier;
	}

	public void setEcheancier(Date echeancier) {
		this.echeancier = echeancier;
	}

	public Double getTauxAtteinte() {
		return tauxAtteinte;
	}

	public void setTauxAtteinte(Double tauxAtteinte) {
		this.tauxAtteinte = tauxAtteinte;
	}
	public String getCommentaireCollabMiPar() {
		return commentaireCollabMiPar;
	}
	public void setCommentaireCollabMiPar(String commentaireCollabMiPar) {
		this.commentaireCollabMiPar = commentaireCollabMiPar;
	}
	public String getCommentaireRespMiPar() {
		return commentaireRespMiPar;
	}
	public void setCommentaireRespMiPar(String commentaireRespMiPar) {
		this.commentaireRespMiPar = commentaireRespMiPar;
	}
	public String getCommentaireCollabFin() {
		return commentaireCollabFin;
	}
	public void setCommentaireCollabFin(String commentaireCollabFin) {
		this.commentaireCollabFin = commentaireCollabFin;
	}
	public String getCommentaireRespFin() {
		return commentaireRespFin;
	}
	public void setCommentaireRespFin(String commentaireRespFin) {
		this.commentaireRespFin = commentaireRespFin;
	}
	
	

	
}
