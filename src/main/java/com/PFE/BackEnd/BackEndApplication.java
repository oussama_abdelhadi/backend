package com.PFE.BackEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.PFE.BackEnd")
public class BackEndApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(BackEndApplication.class, args);
	}

}
