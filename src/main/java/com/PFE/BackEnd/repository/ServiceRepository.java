package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PFE.BackEnd.Model.Direction;
import com.PFE.BackEnd.Model.Service;

public interface ServiceRepository extends JpaRepository<Service, Integer>{

	@Query(value = "SELECT * FROM service s WHERE s.intitule_position = ?1", nativeQuery = true)
	public Service findByServiceName (String intitulePosition);
	
	public Service findByCodeService(String codeService);
	List<Service> findByDirection(Direction direction);
	
}
