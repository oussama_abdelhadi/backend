package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Formation;

public interface FormationRepository extends JpaRepository<Formation, Integer>{

	Formation findByNomFormation(String nomFormation);
	List<Formation> findByEmploye(Employe employe);
	
}
