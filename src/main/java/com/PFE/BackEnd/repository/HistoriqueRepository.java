package com.PFE.BackEnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Historique;


public interface HistoriqueRepository extends JpaRepository<Historique, Integer>{

}
