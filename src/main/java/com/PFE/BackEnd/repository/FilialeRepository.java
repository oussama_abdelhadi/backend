package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PFE.BackEnd.Model.Division;
import com.PFE.BackEnd.Model.Filiale;

public interface FilialeRepository extends JpaRepository<Filiale, Integer>{
	@Query(value = "SELECT * FROM filiale f WHERE f.intitule_position = ?1", nativeQuery = true)
	public Filiale findByFilialeName (String intitulePosition);
	public Filiale findByCodeFiliale(String codeFiliale);
	List<Filiale> findByDivision(Division division);
	
}
