package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PFE.BackEnd.Model.Objectif;

public interface ObjectifRepository extends JpaRepository<Objectif, Integer>{

	@Query(value = "SELECT * FROM objectif o WHERE o.nom_objectif = ?1", nativeQuery = true)
	public Objectif findByObjectifName (String nomObjectif);
	
	@Query(value = "SELECT * FROM objectif o WHERE o.type_objectif = 1", nativeQuery = true)
	public Iterable<Objectif> findByObjectifType ();
	
	@Query(value = "SELECT * FROM objectif o WHERE o.position_code_position = ?1", nativeQuery = true)
	public List<Objectif> findByPosition (Integer codePosition);	

}
