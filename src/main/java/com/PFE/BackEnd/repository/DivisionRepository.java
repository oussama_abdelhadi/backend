package com.PFE.BackEnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PFE.BackEnd.Model.Division;

public interface DivisionRepository extends JpaRepository<Division, Integer>{

	@Query(value = "SELECT * FROM division f WHERE f.intitule_position = ?1", nativeQuery = true)
	public Division findByDivisionName (String intitulePosition);
	
	public Division findByCodeDivision(String codeDivision);
}
