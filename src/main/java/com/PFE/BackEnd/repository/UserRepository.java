package com.PFE.BackEnd.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PFE.BackEnd.Model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByEmail(String email);
	@Query(value = "SELECT * FROM user u WHERE u.username = ?1", nativeQuery = true)
	public User findByUsername (String username);
	Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}
