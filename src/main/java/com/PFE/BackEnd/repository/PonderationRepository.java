package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Evaluation;
import com.PFE.BackEnd.Model.Ponderation;

public interface PonderationRepository extends JpaRepository<Ponderation, Integer>{

	List<Ponderation> findByEmploye(Employe employe);
	List<Ponderation> findByEvaluation(Evaluation evaluation);
}
