package com.PFE.BackEnd.repository;

import org.springframework.data.repository.CrudRepository;

import com.PFE.BackEnd.Model.Regle;

public interface RegleRepository extends CrudRepository<Regle, Integer>{

}
