package com.PFE.BackEnd.repository;

import org.springframework.data.repository.CrudRepository;

import com.PFE.BackEnd.Model.Notification;

public interface NotificationRepository extends CrudRepository<Notification, Integer>{

}
