package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Evaluation;
import com.PFE.BackEnd.Model.Objectif;

public interface EvaluationRepository extends JpaRepository<Evaluation, Integer>{
	
	List<Evaluation> findByTypeEvaluation(Boolean typeEvaluation);
	List<Evaluation> findByObjectif(Objectif objectif);
	List<Evaluation> findByAnnee(Integer annee);
	
	
}
