package com.PFE.BackEnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Position;

public interface PositionRepository extends JpaRepository<Position, Integer>{
	
}
