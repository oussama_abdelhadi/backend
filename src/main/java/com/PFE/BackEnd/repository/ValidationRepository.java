package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Validation;

public interface ValidationRepository extends JpaRepository<Validation, Integer>{
	List<Validation> findByEmploye(Employe employe);
	
}
