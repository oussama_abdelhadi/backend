package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.PFE.BackEnd.Model.Direction;
import com.PFE.BackEnd.Model.Filiale;

public interface DirectionRepository extends JpaRepository<Direction, Integer>{

	@Query(value = "SELECT * FROM direction f WHERE f.intitule_position = ?1", nativeQuery = true)
	public Direction findByDirectionName (String intitulePosition);
	
	public Direction findByCodeDirection(String codeDirection);
	
	List<Direction> findByFiliale(Filiale filiale);
	
	
}
