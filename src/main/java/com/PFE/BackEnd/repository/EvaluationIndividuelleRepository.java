package com.PFE.BackEnd.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.EvaluationIndividuelle;

public interface EvaluationIndividuelleRepository extends JpaRepository<EvaluationIndividuelle, Integer>{

	List<EvaluationIndividuelle> findByEmploye(Employe employe);

}
