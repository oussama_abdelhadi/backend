package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Position;
import com.PFE.BackEnd.Model.Poste;

public interface PosteRepository extends JpaRepository<Poste, Integer>{
	List<Poste> findByPosition(Position position);
	
}
