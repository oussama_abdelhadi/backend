package com.PFE.BackEnd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Evolution;

public interface EvolutionRepository extends JpaRepository<Evolution, Integer>{
	
	List<Evolution> findByEmploye(Employe employe);
	
}
