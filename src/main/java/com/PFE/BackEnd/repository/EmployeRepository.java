package com.PFE.BackEnd.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Poste;
import com.PFE.BackEnd.Model.User;

public interface EmployeRepository extends JpaRepository<Employe, Integer>{

	List<Employe> findBySuperieur(Employe superieur);
	Optional<Employe> findByUser(User user);
	List<Employe> findByPoste(Poste poste);
}
