package com.PFE.BackEnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PFE.BackEnd.Model.Phase;

public interface PhaseRepository extends JpaRepository<Phase, Integer>{
}
