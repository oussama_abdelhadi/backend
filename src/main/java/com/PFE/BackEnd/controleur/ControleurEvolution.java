package com.PFE.BackEnd.controleur;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Evolution;
import com.PFE.BackEnd.Model.Phase;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.EmployeRepository;
import com.PFE.BackEnd.repository.EvolutionRepository;
import com.PFE.BackEnd.repository.PhaseRepository;
import com.PFE.BackEnd.service.EvolutionService;

@RequestMapping("Evolution")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurEvolution {

private EvolutionService serv;
	
	@Autowired
	private PhaseRepository phaseRepository;

	@Autowired
	private EmployeRepository employeRepository;
	@Autowired
	private EvolutionRepository evolutionRepository;

	@Autowired
	public ControleurEvolution(EvolutionService serv) {
		this.serv=serv;
	}
	
	
	@PostMapping(path="/add")
	  public ResponseEntity<?> addNewEvolution (@RequestBody Evolution e) {
		

		if (e.getEmploye() == null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Imposible de cree une evolution sans employe!!"),
					HttpStatus.NOT_ACCEPTABLE);
		}
		Phase phase = phaseRepository.findAll().get(0);
		  ArrayList<Evolution> evoList = (ArrayList<Evolution>) evolutionRepository.findByEmploye(employeRepository.findById(e.getEmploye().getCodeEmploye()).get());
		  
		  for (Evolution evo : evoList) {
			  if (evo.getDate().equals(phase.getDate())) {
				  return new ResponseEntity<>(new ResponseMessage("Erreur -> Une evolution existe deja pour cet employé cette année!!"),
							HttpStatus.NOT_ACCEPTABLE);
			  }
		  }
		  evolutionRepository.save(e);
		return ResponseEntity.ok(e);
	  }

	  @GetMapping(path="/all/{id}")
	  public ResponseEntity<?> getAllEvolutions(@PathVariable Integer id) {

		  if (employeRepository.findById(id).isPresent()) {
			  Phase phase = phaseRepository.findAll().get(0);
			  ArrayList<Evolution> evoList = (ArrayList<Evolution>) evolutionRepository.findByEmploye(employeRepository.findById(id).get());
			  
			  for (Evolution evo : evoList) {
				  if (evo.getDate().equals(phase.getDate())) {
					  return ResponseEntity.ok(evo);
				  }
			  }
			  return ResponseEntity.ok(null);
			
		  }
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
					HttpStatus.BAD_REQUEST);
		  
	  }
	  
	  	@PutMapping(path="/update")
		public ResponseEntity<?> update (@RequestBody Evolution e) {
			Optional<Evolution> p = evolutionRepository.findById(e.getCodeEvolution());
	        
	        if (p.isPresent()) {
	        	Evolution evolution = p.get();
	        	evolution.setAvisResp(e.getAvisResp());
	        	evolution.setCommentaireCol(e.getCommentaireCol());
	        	evolution.setEcheanceEvolution(e.getEcheanceEvolution());
	        	evolution.setPosteSouhaite(e.getPosteSouhaite());
	        	evolution.setPreferenceGeo(e.getPreferenceGeo());
	        	evolution.setType(e.getType());
	        	evolution.setVoteResp(e.getVoteResp());
	        	evolutionRepository.save(evolution);
				return ResponseEntity.ok(e);
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> La formation n'existe pas!"),
					HttpStatus.NOT_FOUND);
		}
	  	
	  	@DeleteMapping("delete/{id}")
	    public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	    {
			Optional<Evolution> f = evolutionRepository.findById(id);
			if (f.isPresent()) {
	        	try {
	        		Evolution evolution = f.get();
	        		evolutionRepository.delete(evolution);
	    			return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
	    					HttpStatus.OK);
	        	}catch(Exception e) {
	        		return new ResponseEntity<>(new ResponseMessage("" + e.getStackTrace()),
	        				HttpStatus.FORBIDDEN);
	        	}
				
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> la formation n'existe pas!"),
					HttpStatus.NOT_FOUND);
	    }
	
}
