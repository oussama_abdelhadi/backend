package com.PFE.BackEnd.controleur;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Objectif;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.ObjectifRepository;
import com.PFE.BackEnd.service.ObjectifService;

@RequestMapping("Objectif")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurObjectif {
	
	private ObjectifService objectifService;

	@Autowired
	private ObjectifRepository objectifRepository;
	
	@Autowired
	public ControleurObjectif(ObjectifService objectifService) {
		this.objectifService = objectifService;
	}
	
	@PostMapping(path="/add")
	  public @ResponseBody Objectif addNewObjectif (@RequestBody Objectif o) {
		objectifService.save(o);
	    return o;
	  }

	  @GetMapping(path="/all")
	  public ResponseEntity<?> getAllObjectifs() {
		  return ResponseEntity.ok(objectifRepository.findAll());
	  }
	  
	  
	  
	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Objectif o) {
		 
		  Optional<Objectif> obj = objectifRepository.findById(o.getCodeObjectif());
			if (obj.isPresent()) {
				Objectif objectif = obj.get();
				objectif.setNomObjectif(o.getNomObjectif());
				objectif.setTypeObjectif(o.getTypeObjectif());
				objectif.setPosition(o.getPosition());
				objectifRepository.save(o);
				return ResponseEntity.ok(o);

			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Objectif n'existe pas!"),
						HttpStatus.NOT_FOUND);
			}
	  }
	  
	  @DeleteMapping("delete/{id}")
	  public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	  {
		  Optional<Objectif> obj = objectifRepository.findById(id);
		  if (obj.isPresent()) {
			  
			  objectifRepository.delete(obj.get());
			  return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
						HttpStatus.OK);
	            
	        }
	  
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Objectif n'existe pas!"),
					HttpStatus.NOT_FOUND);
	  }
	
}
