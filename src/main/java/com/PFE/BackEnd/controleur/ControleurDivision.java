package com.PFE.BackEnd.controleur;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Division;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.DivisionRepository;
import com.PFE.BackEnd.repository.FilialeRepository;
import com.PFE.BackEnd.repository.ObjectifRepository;
import com.PFE.BackEnd.repository.PosteRepository;
import com.PFE.BackEnd.service.DivisionService;

@RequestMapping("Division")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurDivision {


	
	private DivisionService divisionService;
	@Autowired
	private PosteRepository posteRepository;

	@Autowired
	private ObjectifRepository objectifRepository;
	@Autowired
	private FilialeRepository filialeRepository;
	@Autowired
	private DivisionRepository divisionRepository;

	@Autowired
	public ControleurDivision(DivisionService divisionService) {
		this.divisionService=divisionService;
	}
	
	
	@PostMapping(path="/add")
	  public  ResponseEntity<?>  addNewDivision (@RequestBody Division e) {
		if (divisionRepository.findByDivisionName(e.getIntitulePosition())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Division Name deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (divisionRepository.findByCodeDivision(e.getCodeDivision())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Code Division deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		
		divisionRepository.save(e);
		/*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userRepository.findByUsername(auth.getName());
		System.out.println(auth.getName());
		Historique h = new Historique("Creation du division " + e.getIntitulePosition() , new Date(), user);
		historiqueRepository.save(h);*/
		return ResponseEntity.ok(e);
	  }

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Division> getAllDivisions() {
	    return divisionService.findAll();
	  }
	  
	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Division e) {
		  
		  Optional<Division> div = divisionRepository.findById(e.getCodePosition());
			if (div.isPresent()) {
				Division division = div.get();
				division.setCodeDivision(e.getCodeDivision());
				division.setIntitulePosition(e.getIntitulePosition());
				divisionService.save(e);
				return ResponseEntity.ok(e);

			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Division n'existe pas!"),
						HttpStatus.NOT_FOUND);
			}
	  }
	  
	  @DeleteMapping("delete/{id}")
	    public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	    {
			Optional<Division> div = divisionRepository.findById(id);
			if (div.isPresent()) {
				if (objectifRepository.findByPosition(div.get().getCodePosition()).isEmpty()) {
					if (filialeRepository.findByDivision(div.get()).isEmpty()) {
						if (posteRepository.findByPosition(div.get()).isEmpty()) {
							divisionRepository.delete(div.get());
							return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
									HttpStatus.OK);
						}
						return new ResponseEntity<>(new ResponseMessage("Erreur -> Des postes ont deja cette position, il faut les changer avant de le supprimé!"),
								HttpStatus.FORBIDDEN);
					}
					return new ResponseEntity<>(new ResponseMessage("Erreur -> Des filiales ont deja cette position, il faut les changer avant de le supprimé!"),
							HttpStatus.FORBIDDEN);
				}
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Des objectifs ont deja cette position, il faut les changer avant de le supprimé!"),
						HttpStatus.FORBIDDEN);
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> Division n'existe pas!"),
					HttpStatus.NOT_FOUND);
	    }
}
