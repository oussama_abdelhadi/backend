package com.PFE.BackEnd.controleur;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Phase;
import com.PFE.BackEnd.Model.Validation;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.EmployeRepository;
import com.PFE.BackEnd.repository.PhaseRepository;
import com.PFE.BackEnd.repository.ValidationRepository;

@RequestMapping("Validation")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurValidation {

	@Autowired
	private EmployeRepository employeRepository;
	@Autowired
	private ValidationRepository validationRepository;
	@Autowired
	private PhaseRepository phaseRepository;
	
	@PostMapping(path="/add")
	public ResponseEntity<?> addNewValidation (@RequestBody Validation v) {
		if (v.getEmploye()!=null) {
			
			if (employeRepository.findById(v.getEmploye().getCodeEmploye()).isPresent()) {
				Phase phase = phaseRepository.findAll().get(0);
				ArrayList<Validation> valList = (ArrayList<Validation>) validationRepository.findByEmploye(employeRepository.findById(v.getEmploye().getCodeEmploye()).get());
				  for (Validation val : valList) {
					  if (val.getAnneeVal().equals(phase.getDate()) && val.getTypeVal().equals(v.getTypeVal())) {
						  return new ResponseEntity<>(new ResponseMessage("Erreur -> une validation existe deja cette année!!"),
									HttpStatus.BAD_REQUEST);
					  }
				  }
					
				v.setAnneeVal(phase.getDate());
				validationRepository.save(v);
				return ResponseEntity.ok(v);	
			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Le code d'employe envoyer n'est pas trouve!!"),
						HttpStatus.NOT_FOUND);
			}
		}else {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Impossible d'ajouter une ponderation sans employe!"),
					HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@GetMapping(path="/getMi/{id}")
	public ResponseEntity<?> getValidationMi(@PathVariable Integer id) {
		
		if (employeRepository.findById(id).isPresent()) {
			Phase phase = phaseRepository.findAll().get(0);
			ArrayList<Validation> valList = (ArrayList<Validation>) validationRepository.findByEmploye(employeRepository.findById(id).get());
			  for (Validation val : valList) {
				  if (val.getAnneeVal().equals(phase.getDate()) && val.getTypeVal() == 0) {
					  return ResponseEntity.ok(val);
				  }
			  }
			  return new ResponseEntity<>(new ResponseMessage("Erreur -> Aucune validation créer pour cet employé cette année!!"),
						HttpStatus.NOT_FOUND);
				
		}
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(path="/getFin/{id}")
	public ResponseEntity<?> getValidationFin(@PathVariable Integer id) {
		
		if (employeRepository.findById(id).isPresent()) {
			Phase phase = phaseRepository.findAll().get(0);
			ArrayList<Validation> valList = (ArrayList<Validation>) validationRepository.findByEmploye(employeRepository.findById(id).get());
			  for (Validation val : valList) {
				  if (val.getAnneeVal().equals(phase.getDate())&& val.getTypeVal() == 1) {
					  return ResponseEntity.ok(val);
				  }
			  }
			  return new ResponseEntity<>(new ResponseMessage("Erreur -> Aucune validation créer pour cet employé cette année!!"),
						HttpStatus.NOT_FOUND);
				
		}
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(path="/getByPhase/{id}")
	public ResponseEntity<?> getValidationParPhase(@PathVariable Integer id) {
		
		if (employeRepository.findById(id).isPresent()) {
			Phase phase = phaseRepository.findAll().get(0);
			ArrayList<Validation> valList = (ArrayList<Validation>) validationRepository.findByEmploye(employeRepository.findById(id).get());
			
			if (phase.getEtape() <= 7) {
				for (Validation val : valList) {
					  if (val.getAnneeVal().equals(phase.getDate())&& val.getTypeVal() == 0) {
						  return ResponseEntity.ok(val);
					  }
				}
			}else {
				for (Validation val : valList) {
					  if (val.getAnneeVal().equals(phase.getDate())&& val.getTypeVal() == 1) {
						  return ResponseEntity.ok(val);
					  }
				}
			}
			
			
			  return new ResponseEntity<>(new ResponseMessage("Erreur -> Aucune validation créer pour cet employé cette année!!"),
						HttpStatus.NOT_FOUND);
				
		}
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	
	@PutMapping(path="/update")
	public ResponseEntity<?> update (@RequestBody Validation e) {
		Optional<Validation> v = validationRepository.findById(e.getCodeValidation());
        
        if (v.isPresent()) {
        	Validation validation = v.get();
        	validation.setTypeVal(e.getTypeVal());
        	validation.setValCol(e.getValCol());
        	validation.setValResp(e.getValResp());
        	validation.setDateCol(e.getDateCol());
        	validation.setDateResp(e.getDateResp());
        	validation.setPointFrot(e.getPointFrot());
        	validation.setPointAm(e.getPointAm());
        	validation.setTro(e.getTro());
        	validation.setCommentaireCol(e.getCommentaireCol());
        	validation.setCommentaireResp(e.getCommentaireResp());
        	validationRepository.save(validation);
			return ResponseEntity.ok(e);
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> La Validation n'existe pas!"),
				HttpStatus.NOT_FOUND);
	}
}
