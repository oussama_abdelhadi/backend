package com.PFE.BackEnd.controleur;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Direction;
import com.PFE.BackEnd.Model.Filiale;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.DirectionRepository;
import com.PFE.BackEnd.repository.FilialeRepository;
import com.PFE.BackEnd.repository.ObjectifRepository;
import com.PFE.BackEnd.repository.PosteRepository;
import com.PFE.BackEnd.repository.ServiceRepository;

@RequestMapping("Direction")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurDirection {

	private DirectionRepository directionRepository;

	@Autowired
	private FilialeRepository filialeRepository;
	@Autowired
	private PosteRepository posteRepository;

	@Autowired
	private ObjectifRepository objectifRepository;
	@Autowired
	private ServiceRepository serviceRepository;
	@Autowired
	public ControleurDirection( DirectionRepository serD) {
		this.directionRepository=serD;
	}
	
	
	@PostMapping(path="/add")
	  public ResponseEntity<?> addNewCompetence (@RequestBody Direction e) {
		if (directionRepository.findByDirectionName(e.getIntitulePosition())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Direction Name deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (directionRepository.findByCodeDirection(e.getCodeDirection())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Code Direction deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (e.getFiliale()!=null) {
			Filiale filiale = filialeRepository.findByFilialeName(e.getFiliale().getIntitulePosition());
			if (filiale ==null) {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Filiale n'existe pas!"),
						HttpStatus.BAD_REQUEST);
			}
		}
		
		directionRepository.save(e);
		return ResponseEntity.ok(e);
	  }

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Direction> getAllCompetences() {
	    return directionRepository.findAll();
	  }
	  
	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Direction e) {
		  Optional<Direction> dir = directionRepository.findById(e.getCodePosition());
			if (dir.isPresent()) {
				Direction direction = dir.get();
				direction.setCodeDirection(e.getCodeDirection());
				direction.setIntitulePosition(e.getIntitulePosition());
				direction.setFiliale(e.getFiliale());
				directionRepository.save(direction);
				return ResponseEntity.ok(e);

			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> direction n'existe pas!"),
						HttpStatus.BAD_REQUEST);
			}
	  }
	  
	  @DeleteMapping("delete/{id}")
	    public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	    {
			Optional<Direction> dir = directionRepository.findById(id);
			if (dir.isPresent()) {
				if (objectifRepository.findByPosition(dir.get().getCodePosition()).isEmpty()) {
					if (serviceRepository.findByDirection(dir.get()).isEmpty()) {
						if (posteRepository.findByPosition(dir.get()).isEmpty()) {
							directionRepository.delete(dir.get());
							return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
									HttpStatus.OK);
						}
						return new ResponseEntity<>(new ResponseMessage("Erreur -> Des postes ont deja cette position, il faut les changer avant de le supprimé!"),
								HttpStatus.FORBIDDEN);
					}
					return new ResponseEntity<>(new ResponseMessage("Erreur -> Des services ont deja cette position, il faut les changer avant de le supprimé!"),
							HttpStatus.FORBIDDEN);
				}
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Des objectifs ont deja cette position, il faut les changer avant de le supprimé!"),
						HttpStatus.FORBIDDEN);
			
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> Direction n'existe pas!"),
					HttpStatus.NOT_FOUND);
	    }
}
