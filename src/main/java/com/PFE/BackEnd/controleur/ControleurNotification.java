package com.PFE.BackEnd.controleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Notification;
import com.PFE.BackEnd.service.NotificationService;

@RequestMapping("api/notifications")
@RestController
public class ControleurNotification {

private NotificationService serv;
	
	@Autowired
	public ControleurNotification(NotificationService serv) {
		this.serv=serv;
	}
	
	
	@PostMapping(path="/add")
	  public @ResponseBody String addNewNotification (@RequestBody Notification e) {
		serv.save(e);
	    return "Saved";
	  }

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Notification> getAllNotifications() {
	    return serv.findAll();
	  }
	
}
