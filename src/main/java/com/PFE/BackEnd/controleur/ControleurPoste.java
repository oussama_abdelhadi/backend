package com.PFE.BackEnd.controleur;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Position;
import com.PFE.BackEnd.Model.Poste;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.EmployeRepository;
import com.PFE.BackEnd.repository.PositionRepository;
import com.PFE.BackEnd.repository.PosteRepository;
import com.PFE.BackEnd.service.PosteService;

@RequestMapping("Poste")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurPoste {

	@Autowired
	private PosteService posteService;
	
	@Autowired
	private PosteRepository posteRepository;
	
	@Autowired
	private PositionRepository positionRepository;
	
	@Autowired
	private EmployeRepository employeRepository;
	
	@PostMapping(path="/add")
	  public ResponseEntity<?> addNewPoste (@RequestBody Poste e) {
		if (e.getPosition() == null) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Aucune Position!!"),
					HttpStatus.BAD_REQUEST);
		}else if (e.getIntitulePoste() == null || e.getIntitulePoste().equals("")) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Aucun intitule de poste!!"),
					HttpStatus.BAD_REQUEST);
		}
		
		Optional<Position> p = positionRepository.findById(e.getPosition().getCodePosition());
		
		if (p.isPresent()) {
			Position position = p.get();
			ArrayList<Poste> postes = (ArrayList<Poste>) posteRepository.findByPosition(position);
			
			for (Poste pst : postes) {
				if (pst.getIntitulePoste().equals(e.getIntitulePoste())) {
					return new ResponseEntity<>(new ResponseMessage("Fail -> Impossible d'ajouter 2 postes de meme intitule sur la meme position!!"),
							HttpStatus.BAD_REQUEST);
				}
			}
			
			
		}else {
			return new ResponseEntity<>(new ResponseMessage("Fail -> la position envoye est fausse!!"),
					HttpStatus.BAD_REQUEST);
		}
		
		posteService.save(e);
		return ResponseEntity.ok(e);
	  }

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Poste> getAllPostes() {
	    return posteService.findAll();
	  }
	  
	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Poste e) {
		  Optional<Poste> p = posteRepository.findById(e.getCodePoste());
			if (p.isPresent()) {
				Poste poste = p.get();
				poste.setIntitulePoste(e.getIntitulePoste());
				poste.setCategoriePoste(e.getCategoriePoste());
				poste.setPosition(e.getPosition());
				posteRepository.save(e);
				return ResponseEntity.ok(e);

			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Poste n'existe pas!"),
						HttpStatus.NOT_FOUND);
			}
	  }
	  
	  @DeleteMapping("delete/{id}")
	  public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	  {
		  Optional<Poste> poste = posteRepository.findById(id);
		  if (poste.isPresent()) {
			  if (employeRepository.findByPoste(poste.get()).isEmpty()) {
				  posteRepository.delete(poste.get());
				  return new ResponseEntity<>(new ResponseMessage("Deleted!"),
							HttpStatus.OK);
			  }
			  
			  return new ResponseEntity<>(new ResponseMessage("Erreur -> Des employé ont deja ce poste, il faut les changer de poste avant de le supprimé!"),
						HttpStatus.FORBIDDEN);
	            
	        }
	  
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Poste n'existe pas!"),
					HttpStatus.NOT_FOUND);
	  }
	  
	  @DeleteMapping("deleteWithListOfEmploye/{id}")
	  public ResponseEntity<?> deleteWithListOfEmploye(@PathVariable("id") Integer id)
	  {
		  Optional<Poste> poste = posteRepository.findById(id);
		  if (poste.isPresent()) {
			  if (employeRepository.findByPoste(poste.get()).isEmpty()) {
				  posteRepository.delete(poste.get());
				  return new ResponseEntity<>(new ResponseMessage("Deleted!"),
							HttpStatus.OK);
			  }
			  
			  return new ResponseEntity<>(employeRepository.findByPoste(poste.get()),
						HttpStatus.OK);
	            
	        }
	  
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Poste n'existe pas!"),
					HttpStatus.NOT_FOUND);
	  }

	  
}
