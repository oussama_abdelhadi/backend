package com.PFE.BackEnd.controleur;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Direction;
import com.PFE.BackEnd.Model.Division;
import com.PFE.BackEnd.Model.Evaluation;
import com.PFE.BackEnd.Model.EvaluationIndividuelle;
import com.PFE.BackEnd.Model.Filiale;
import com.PFE.BackEnd.Model.Objectif;
import com.PFE.BackEnd.Model.Phase;
import com.PFE.BackEnd.Model.Ponderation;
import com.PFE.BackEnd.Model.Position;
import com.PFE.BackEnd.Model.Service;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.EmployeRepository;
import com.PFE.BackEnd.repository.EvaluationIndividuelleRepository;
import com.PFE.BackEnd.repository.EvaluationRepository;
import com.PFE.BackEnd.repository.ObjectifRepository;
import com.PFE.BackEnd.repository.PhaseRepository;
import com.PFE.BackEnd.repository.PonderationRepository;
import com.PFE.BackEnd.repository.PositionRepository;
import com.PFE.BackEnd.service.EvaluationService;

@RequestMapping("Evaluation")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurEvaluation {
	
	private EvaluationService evaS;
	
	@Autowired
	private ObjectifRepository objectifRepository;
	@Autowired
	private EmployeRepository employeRepository;
	@Autowired
	private EvaluationRepository evaluationRepository;
	@Autowired
	private PositionRepository positionRepository;
	@Autowired
	private EvaluationIndividuelleRepository evaluationIndividuelleRepository;
	@Autowired
	private PhaseRepository phaseRepository;
	@Autowired
	private PonderationRepository ponderationRepository;
	@Autowired
	public ControleurEvaluation(EvaluationService evaS) {
		this.evaS=evaS;
	}
	
	@GetMapping(path="/evaCollect/{id}")
	public ResponseEntity<?> getEvalsCollect(@PathVariable Integer id) {
		if (id == null){
			return new ResponseEntity<>(new ResponseMessage("Fail -> aucun codePosition!"),
					HttpStatus.BAD_REQUEST);
		}
		Optional<Position> position = positionRepository.findById(id);
		if (position.isPresent()) {
			ArrayList<Evaluation> listEva = (ArrayList<Evaluation>) evaluationRepository.findByTypeEvaluation(true);
			List<Evaluation> evaluations = new ArrayList<Evaluation>();
			Service service;
			Direction direction;
			Filiale filiale;
			Division division;
			
			if (position.get().getType().equals("service")) {
				
				service = (Service) position.get();
				
				if (service.getDirection() == null) {
					
					for (Evaluation eva : listEva) {
						if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
							return ResponseEntity.ok(evaluations);
						}
					}
					
					return new ResponseEntity<>(new ResponseMessage("Fail -> Il n'existe aucune evaluation groupe + service sans direction!!"),
							HttpStatus.BAD_REQUEST);
					
				}else if(service.getDirection().getFiliale() == null ) { // direction sans filiale
					
					for (Evaluation eva : listEva) {
						if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
							return ResponseEntity.ok(evaluations);
						}
					}
					
					return new ResponseEntity<>(new ResponseMessage("Fail -> Il n'existe aucune evaluation groupe + direction de service sans filiale!!"),
							HttpStatus.BAD_REQUEST);
					
				}else if(service.getDirection().getFiliale().getDivision() == null){ // filiale sans division
					
					for (Evaluation eva : listEva) {
						if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
						}
					}
					for (Evaluation eva : listEva) {
						if (eva.getObjectif().getPosition() != null) {
							if (eva.getObjectif().getPosition().getCodePosition() == service.getDirection().getFiliale().getCodePosition() && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
								evaluations.add(eva);
							}
						}
						
					}
					return ResponseEntity.ok(evaluations); 
					
				}else {
					filiale = service.getDirection().getFiliale();
				}
				
			}else if (position.get().getType().equals("direction")) {
				
				direction = (Direction) position.get();
				
				if(direction.getFiliale() == null ) {
						
					for (Evaluation eva : listEva) {
						if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
							return ResponseEntity.ok(evaluations);
						}
					}
					
					return new ResponseEntity<>(new ResponseMessage("Fail -> Il n'existe aucune evaluation groupe + direction sans filiale!!"),
							HttpStatus.BAD_REQUEST);
				}else if(direction.getFiliale().getDivision() == null ) {
					
					for (Evaluation eva : listEva) {
						if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
						}
					}
					for (Evaluation eva : listEva) {
						if (eva.getObjectif().getPosition() != null) {
							if (eva.getObjectif().getPosition().getCodePosition() == direction.getFiliale().getCodePosition() && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
								evaluations.add(eva);
							}
						}
						
					}
					return ResponseEntity.ok(evaluations);
					
					
				}else {
					filiale = direction.getFiliale();
				}

			}else if (position.get().getType().equals("filiale")){
				
				filiale = (Filiale) position.get();
				
				if(filiale.getDivision() == null ) {
				
					for (Evaluation eva : listEva) {
						if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
						}
					}
					for (Evaluation eva : listEva) {
						if (eva.getObjectif().getPosition() != null) {
							if (eva.getObjectif().getPosition().getCodePosition() == filiale.getCodePosition() && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
								evaluations.add(eva);
							}
						}
						
					}
					return ResponseEntity.ok(evaluations);
					
				
				}
			
			}else {
				division = (Division) position.get();
				
				for (Evaluation eva : listEva) {
					if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
						evaluations.add(eva);
					}
				}
				for (Evaluation eva : listEva) {
					if (eva.getObjectif().getPosition() != null) {
						if (eva.getObjectif().getPosition().getCodePosition() == division.getCodePosition() && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							evaluations.add(eva);
						}
					}
					
				}
				
				return ResponseEntity.ok(evaluations); 
			}
			
			
			
			for (Evaluation eva : listEva) {
				if ((eva.getObjectif().getPosition() == null) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
					evaluations.add(eva);
				}
			}
			for (Evaluation eva : listEva) {
				if (eva.getObjectif().getPosition() != null) {
					if (eva.getObjectif().getPosition().getCodePosition() == filiale.getDivision().getCodePosition() && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
						evaluations.add(eva);
					}
				}
				
			}
			for (Evaluation eva : listEva) {
				if (eva.getObjectif().getPosition() != null) {
					if (eva.getObjectif().getPosition().getCodePosition() == filiale.getCodePosition() && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
						evaluations.add(eva);
					}
				}
				
			}
			
			return ResponseEntity.ok(evaluations); 
		}
		return new ResponseEntity<>(new ResponseMessage("Erreur -> aucune Position avec ce codePosition!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(path="/add")
	public ResponseEntity<?> addNewEvaluation (@RequestBody Evaluation e) {
		if (e.getObjectif()!=null) {
			if (e.getAnnee() == null) {
				e.setAnnee(phaseRepository.findAll().get(0).getDate());
			}
			
			ArrayList<Evaluation> listEva = (ArrayList<Evaluation>) evaluationRepository.findAll();
			
			if (e.getObjectif().getPosition() == null ) {
				
				for (Evaluation eva : listEva) {
					if (eva.getObjectif().getPosition() == null && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
						return new ResponseEntity<>(new ResponseMessage("Erreur -> une evaluation globale existe deja cette annee!!"),
		        				HttpStatus.FORBIDDEN);
					}
				}
			}else {
				for (Evaluation eva : listEva) {
					if (eva.getObjectif().getPosition() != null) {
						if ( eva.getObjectif().getPosition().getCodePosition().equals(e.getObjectif().getPosition().getCodePosition())  && ( eva.getObjectif().getPosition().getType().equals("division") || (eva.getObjectif().getPosition().getType().equals("filiale"))) && (eva.getAnnee().equals(phaseRepository.findAll().get(0).getDate()))) {
							return new ResponseEntity<>(new ResponseMessage("Erreur -> une evaluation " + eva.getObjectif().getPosition().getType() + " : '" + eva.getObjectif().getPosition().getIntitulePosition() + "' existe deja cette annee!!"),
			        				HttpStatus.FORBIDDEN);
						}
					}
					
				}
			}
			
			
			if (e.getObjectif().getCodeObjectif() == null){
				Objectif objectif = objectifRepository.findByObjectifName(e.getObjectif().getNomObjectif());
				if (objectif ==null) {
					e.getObjectif().setTypeObjectif(true);
					objectifRepository.save(e.getObjectif());
					objectif = objectifRepository.findByObjectifName(e.getObjectif().getNomObjectif());
					e.setObjectif(objectif);
				}else {
					e.setObjectif(objectif);
				}
			}else {
				Optional<Objectif> objectif = objectifRepository.findById(e.getObjectif().getCodeObjectif());
				if (objectif.isPresent()) {
					e.setObjectif(objectif.get());
					
				}else {
					if (e.getObjectif().getNomObjectif() == null || e.getObjectif().getNomObjectif() == "") {
						return new ResponseEntity<>(new ResponseMessage("Erreur -> aucun nom d'objectif!!"),
		        				HttpStatus.FORBIDDEN);
					}else {
						e.getObjectif().setTypeObjectif(true);
						objectifRepository.save(e.getObjectif());
						Objectif obj = objectifRepository.findByObjectifName(e.getObjectif().getNomObjectif());
						e.setObjectif(obj);
					}
					
				}
			}
			
		
			e.setTypeEvaluation(true);
			evaluationRepository.save(e);
			return ResponseEntity.ok(e);
		}else {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> aucun objectif avec cette evaluation!"),
					HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(path="/all")
	public ResponseEntity<?> getAllEvaluations() {
		int i = phaseRepository.findAll().get(0).getDate();
		return ResponseEntity.ok(evaluationRepository.findByAnnee(i));
	}
	
	@GetMapping(path="/allEmp/{id}")
	public ResponseEntity<?> getAllEmployeEvaluations(@PathVariable Integer id) {
	
		if (employeRepository.findById(id).isPresent()) {
			  Phase phase = phaseRepository.findAll().get(0);
			  ArrayList<EvaluationIndividuelle> evaList = (ArrayList<EvaluationIndividuelle>) evaluationIndividuelleRepository.findByEmploye(employeRepository.findById(id).get());
			  ArrayList<EvaluationIndividuelle> newList = new ArrayList<EvaluationIndividuelle>();
			  for (EvaluationIndividuelle eva : evaList) {
				  if (eva.getAnnee().equals(phase.getDate())) {
					  newList.add(eva);
				  }
			  }
			  return ResponseEntity.ok(newList);
		  }
		
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(path="/addEI")
	public ResponseEntity<?> addNewEvaluationIndividuelle (@RequestBody EvaluationIndividuelle e) {
		if (e.getObjectif()!=null) {
			if (e.getAnnee() == null) {
				e.setAnnee(phaseRepository.findAll().get(0).getDate());
			}
			Objectif objectif = objectifRepository.findByObjectifName(e.getObjectif().getNomObjectif());
			if (objectif ==null) {
				e.getObjectif().setTypeObjectif(false);
				objectifRepository.save(e.getObjectif());
				objectif = objectifRepository.findByObjectifName(e.getObjectif().getNomObjectif());
				e.setObjectif(objectif);
			}else {
				e.setObjectif(objectif);
			}
		}
		e.setTypeEvaluation(false);
		evaS.saveE(e);
		return ResponseEntity.ok(e);
	}
	
	@PostMapping(path="/addPonderation")
	public ResponseEntity<?> addNewPonderation (@RequestBody Ponderation p) {
		if (p.getEmploye()!=null) {
			
			if (employeRepository.findById(p.getEmploye().getCodeEmploye()).isPresent()) {
				if (p.getEvaluation() != null) {
					if (evaluationRepository.findById(p.getEvaluation().getCodeEvaluation()).isPresent()) {
						ponderationRepository.save(p);
						return ResponseEntity.ok(p);
					}else {
						return new ResponseEntity<>(new ResponseMessage("Erreur -> Le code d'evaluation envoyer n'est pas trouve!!"),
								HttpStatus.NOT_FOUND);
					}
				}else {
					return new ResponseEntity<>(new ResponseMessage("Erreur -> Impossible d'ajouter une ponderation sans evaluation!"),
							HttpStatus.BAD_REQUEST);
				}
			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Le code d'employe envoyer n'est pas trouve!!"),
						HttpStatus.NOT_FOUND);
			}
		}else {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Impossible d'ajouter une ponderation sans employe!"),
					HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@GetMapping(path="/allPonderation/{id}")
	public ResponseEntity<?> getAllEmployePonderations(@PathVariable Integer id) {
		if (employeRepository.findById(id).isPresent()) {
			  Phase phase = phaseRepository.findAll().get(0);
			  ArrayList<Ponderation> pondList = (ArrayList<Ponderation>) ponderationRepository.findByEmploye(employeRepository.findById(id).get());
			  ArrayList<Ponderation> newList = new ArrayList<Ponderation>();
			  
			  for (Ponderation pond : pondList) {
				  if (pond.getEvaluation().getAnnee().equals(phase.getDate())) {
					  newList.add(pond);
				  }
			  }
			  return ResponseEntity.ok(newList);
		  }
		
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@PutMapping(path="/updatePonderation")
	public ResponseEntity<?> updatePonderation (@RequestBody Ponderation e) {
		Optional<Ponderation> p = ponderationRepository.findById(e.getId());
        
        if (p.isPresent()) {
        	Ponderation ponderation = p.get();
        	ponderation.setPonderation(e.getPonderation());
        	ponderation.setPlanAction(e.getPlanAction());
        	
        	ponderationRepository.save(ponderation);
			return ResponseEntity.ok(e);
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> La ponderation n'existe pas!"),
				HttpStatus.NOT_FOUND);
	}

	
	@GetMapping(path="/check")
	public ResponseEntity<?> checkAllPonderations() {
		ArrayList<Ponderation> ponderations = (ArrayList<Ponderation>) ponderationRepository.findAll();
		ArrayList<Ponderation> listPondNonRemplis = new ArrayList<Ponderation>();
		Phase phase = phaseRepository.findAll().get(0);
		boolean b = true;
		for (Ponderation p : ponderations) {
			if (p.getEvaluation().getAnnee().equals(phase.getDate())) {
				if (p.getPonderation() == null) {
					b = false;
					listPondNonRemplis.add(p);
				}
			}
		}
		return ResponseEntity.ok(b);
	}
	
	@GetMapping(path="/getSommePonderations/{id}")
	public ResponseEntity<?> getSommePonderations(@PathVariable Integer id) {
		
		if (employeRepository.findById(id).isPresent()) {
			ArrayList<Ponderation> pondList = (ArrayList<Ponderation>) ponderationRepository.findByEmploye(employeRepository.findById(id).get());
			Phase phase = phaseRepository.findAll().get(0);
			double i = 0;
			for (Ponderation p : pondList) {
				if (p.getEvaluation().getAnnee().equals(phase.getDate())) {
					if (p.getPonderation() != null) {
						i = i + p.getPonderation();
					}
				}
			}
			
			ArrayList<EvaluationIndividuelle> evaList = (ArrayList<EvaluationIndividuelle>) evaluationIndividuelleRepository.findByEmploye(employeRepository.findById(id).get());
			  for (EvaluationIndividuelle eva : evaList) {
				  if (eva.getAnnee().equals(phase.getDate())) {
					  if (eva.getPonderation() != null) {
						  i = i + eva.getPonderation();
					  }
				  }
			  }
			return ResponseEntity.ok(i);
		}
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(path="/getTRG/{id}")
	public ResponseEntity<?> getTRG(@PathVariable Integer id) {
		
		if (employeRepository.findById(id).isPresent()) {
			ArrayList<Ponderation> pondList = (ArrayList<Ponderation>) ponderationRepository.findByEmploye(employeRepository.findById(id).get());
			Phase phase = phaseRepository.findAll().get(0);
			double i = 0;
			for (Ponderation p : pondList) {
				if (p.getEvaluation().getAnnee().equals(phase.getDate())) {
					if (p.getPonderation() != null) {
						if (phase.getEtape() <= 7 ) {
							if (p.getEvaluation().getEvalMiParcours() != null)
								i = i + (p.getPonderation() * p.getEvaluation().getEvalMiParcours());
						}else {
							if (p.getEvaluation().getEvalFinale() != null)
								i = i + (p.getPonderation() * p.getEvaluation().getEvalFinale());
						}
						
					}
				}
			}
			
			ArrayList<EvaluationIndividuelle> evaList = (ArrayList<EvaluationIndividuelle>) evaluationIndividuelleRepository.findByEmploye(employeRepository.findById(id).get());
			  for (EvaluationIndividuelle eva : evaList) {
				  if (eva.getAnnee().equals(phase.getDate())) {
					  if (phase.getEtape() <= 7 ) {
						  if (eva.getPonderation() != null && eva.getEvalMiParcours() != null) {
							  
							  i = i + (eva.getPonderation() * eva.getEvalMiParcours());
						  }
					  }else {
						  if (eva.getPonderation() != null && eva.getEvalFinale() != null) {
							  
							  i = i + (eva.getPonderation() * eva.getEvalFinale());
						  }
					  }
					  
				  }
			  }
			  i = i / 100;
			return ResponseEntity.ok(i);
		}
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(path="/allEI")
	public @ResponseBody Iterable<EvaluationIndividuelle> getAllEvaluationIndividuelle() {
		return evaS.findAllEI();
	}
		
	@PutMapping(path="/update")
	public ResponseEntity<?> update (@RequestBody Evaluation e) {
		Optional<Evaluation> eva = evaluationRepository.findById(e.getCodeEvaluation());
        
        if (eva.isPresent()) {
        	Evaluation evaluation = eva.get();
        	evaluation.setKPI(e.getKPI());
        	evaluation.setCible(e.getCible());
        	evaluation.setPlanAction(e.getPlanAction());
        	evaluation.setTypeEvaluation(e.getTypeEvaluation());
        	evaluation.setPonderation(e.getPonderation());
        	evaluation.setEvalMiParcours(e.getEvalMiParcours());
        	evaluation.setEvalFinale(e.getEvalFinale());
        	evaluation.setObjectif(e.getObjectif());
        	evaluation.setAnnee(e.getAnnee());
        	
        	evaluationRepository.save(evaluation);
			return ResponseEntity.ok(e);
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> L'evaluation n'existe pas!"),
				HttpStatus.NOT_FOUND);
	}
	
	@PutMapping(path="/updateEI")
	public ResponseEntity<?> updateEI (@RequestBody EvaluationIndividuelle e) {
		Optional<EvaluationIndividuelle> eva = evaluationIndividuelleRepository.findById(e.getCodeEvaluation());
        
        if (eva.isPresent()) {
        	EvaluationIndividuelle evaluation = eva.get();
        	evaluation.setKPI(e.getKPI());
        	evaluation.setCible(e.getCible());
        	evaluation.setPlanAction(e.getPlanAction());
        	evaluation.setTypeEvaluation(e.getTypeEvaluation());
        	evaluation.setPonderation(e.getPonderation());
        	evaluation.setEvalMiParcours(e.getEvalMiParcours());
        	evaluation.setEvalFinale(e.getEvalFinale());
        	evaluation.setObjectif(e.getObjectif());
        	evaluation.setAnnee(e.getAnnee());
        	evaluation.setEcheancier(e.getEcheancier());
        	evaluation.setEmploye(e.getEmploye());
        	evaluation.setEvalMiParCollab(e.getEvalMiParCollab());
        	evaluation.setEvalFinCollab(e.getEvalFinCollab());
        	evaluation.setTauxAtteinte(e.getTauxAtteinte());
        	evaluation.setCommentaireCollabMiPar(e.getCommentaireCollabMiPar());
        	evaluation.setCommentaireRespMiPar(e.getCommentaireRespMiPar());
        	evaluation.setCommentaireCollabFin(e.getCommentaireCollabFin());
        	evaluation.setCommentaireRespFin(e.getCommentaireRespFin());
        	
        	evaluationIndividuelleRepository.save(evaluation);
			return ResponseEntity.ok(e);
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> L'evaluation n'existe pas!"),
				HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id)
    {
		Optional<Evaluation> eva = evaluationRepository.findById(id);
		if (eva.isPresent()) {
        	
        		Evaluation evaluation = eva.get();
        		ArrayList<Ponderation> ponderations = (ArrayList<Ponderation>) ponderationRepository.findByEvaluation(evaluation);
        		
        		for (Ponderation p : ponderations) {
        			if (p != null) {
        				ponderationRepository.delete(p);
        			}
        		}
        		
        		
        		evaluationRepository.delete(evaluation);
        		
        		Optional<Objectif> obj = objectifRepository.findById(evaluation.getObjectif().getCodeObjectif());
        		if (obj.isPresent()) {
      	        	
      			  objectifRepository.delete(obj.get());
      	            
      	        }
    			return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
    					HttpStatus.OK);
        	
			
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> l'evaluation n'existe pas!"),
				HttpStatus.NOT_FOUND);
    }
	
	@DeleteMapping("deleteEI/{id}")
    public ResponseEntity<?> deleteEI(@PathVariable("id") Integer id)
    {
		Optional<EvaluationIndividuelle> eva = evaluationIndividuelleRepository.findById(id);
		if (eva.isPresent()) {
        	
			evaluationIndividuelleRepository.delete(eva.get());
			return new ResponseEntity<>(new ResponseMessage("Deleted!"),
					HttpStatus.OK);
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> l'evaluation n'existe pas!"),
				HttpStatus.NOT_FOUND);
    }
	
	
	
}
