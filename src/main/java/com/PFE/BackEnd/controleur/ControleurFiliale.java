package com.PFE.BackEnd.controleur;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Division;
import com.PFE.BackEnd.Model.Filiale;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.DirectionRepository;
import com.PFE.BackEnd.repository.DivisionRepository;
import com.PFE.BackEnd.repository.FilialeRepository;
import com.PFE.BackEnd.repository.ObjectifRepository;
import com.PFE.BackEnd.repository.PosteRepository;
import com.PFE.BackEnd.service.FilialeService;

@RequestMapping("Filiale")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurFiliale {

	private FilialeService servF;
	@Autowired
	private DirectionRepository directionRepository;
	
	@Autowired
	private PosteRepository posteRepository;

	@Autowired
	private ObjectifRepository objectifRepository;
	@Autowired
	private FilialeRepository filialeRepository;
	
	
	@Autowired
	private DivisionRepository divisionRepository;
	
	@Autowired
	public ControleurFiliale(FilialeService servF) {
		this.servF= servF;
	}
	
	
	@PostMapping(path="/add")
	  public ResponseEntity<?> addNewFiliale (@RequestBody Filiale e) {
		
		if (filialeRepository.findByFilialeName(e.getIntitulePosition())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Filiale Name deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (filialeRepository.findByCodeFiliale(e.getCodeFiliale())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Code Filiale deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (e.getDivision()!=null) {
			Division div = divisionRepository.findByDivisionName(e.getDivision().getIntitulePosition());
			if (div ==null) {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Division n'existe pas!"),
						HttpStatus.BAD_REQUEST);
			}else {
				e.setDivision(div);
			}
		}
		
		filialeRepository.save(e);
		return ResponseEntity.ok(e);
	  }

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Filiale> getAllFiliales() {
	    return servF.findAll();
	  }
	  
	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Filiale e) {
		  Optional<Filiale> fil = filialeRepository.findById(e.getCodePosition());
			if (fil.isPresent()) {
				Filiale filiale = fil.get();
				filiale.setCodeFiliale(e.getCodeFiliale());
				filiale.setIntitulePosition(e.getIntitulePosition());
				filiale.setDivision(e.getDivision());
				filialeRepository.save(filiale);
				return ResponseEntity.ok(e);

			}else {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Filiale n'existe pas!"),
						HttpStatus.BAD_REQUEST);
			}
	  }
	  
	  @DeleteMapping("delete/{id}")
	  public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	  {
		  Optional<Filiale> fil = filialeRepository.findById(id);
		  if (fil.isPresent()) {
			  if (objectifRepository.findByPosition(fil.get().getCodePosition()).isEmpty()) {
					if (directionRepository.findByFiliale(fil.get()).isEmpty()) {
						if (posteRepository.findByPosition(fil.get()).isEmpty()) {
							filialeRepository.delete(fil.get());
							  return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
										HttpStatus.OK);
						}
						return new ResponseEntity<>(new ResponseMessage("Erreur -> Des postes ont deja cette position, il faut les changer avant de le supprimé!"),
								HttpStatus.FORBIDDEN);
					}
					return new ResponseEntity<>(new ResponseMessage("Erreur -> Des directions ont deja cette position, il faut les changer avant de le supprimé!"),
							HttpStatus.FORBIDDEN);
				}
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Des objectifs ont deja cette position, il faut les changer avant de le supprimé!"),
						HttpStatus.FORBIDDEN);
			            
	        }
	  
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Filiale n'existe pas!"),
					HttpStatus.NOT_FOUND);
	  }
}
