package com.PFE.BackEnd.controleur;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Phase;
import com.PFE.BackEnd.repository.PhaseRepository;

@RequestMapping("Phase")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurPhase {
	
	@Autowired
	private PhaseRepository phaseRepository;
	
	@GetMapping(path="/getPhase")
	  public ResponseEntity<?> getPhase() {
		return ResponseEntity.ok( phaseRepository.findAll().get(0));
	  }
	
	@PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Phase e) {
				Phase phase = phaseRepository.findAll().get(0);
				phase.setDate(e.getDate());
				phase.setEtape(e.getEtape());
				phaseRepository.save(phase);
				return ResponseEntity.ok(e);

	  }


}
