package com.PFE.BackEnd.controleur;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Direction;
import com.PFE.BackEnd.Model.Service;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.DirectionRepository;
import com.PFE.BackEnd.repository.ObjectifRepository;
import com.PFE.BackEnd.repository.PosteRepository;
import com.PFE.BackEnd.repository.ServiceRepository;

@RequestMapping("Service")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurService {

	private ServiceRepository serviceRepository;
	@Autowired
	private PosteRepository posteRepository;
	@Autowired
	private DirectionRepository directionRepository;

	@Autowired
	private ObjectifRepository objectifRepository;
	
	@Autowired
	public ControleurService( ServiceRepository serviceRepository) {
		this.serviceRepository=serviceRepository;
	}
	
	
	@PostMapping(path="/add")
	  public ResponseEntity<?> addNewCompetence (@RequestBody Service e) {
		if (serviceRepository.findByServiceName(e.getIntitulePosition())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Service Name deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (serviceRepository.findByCodeService(e.getCodeService())!=null) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Code Service deja existant!"),
					HttpStatus.BAD_REQUEST);
		}
		if (e.getDirection()!=null) {
			Direction direction = directionRepository.findByDirectionName(e.getDirection().getIntitulePosition());
			if (direction ==null) {
				return new ResponseEntity<>(new ResponseMessage("Fail -> Direction n'existe pas!"),
						HttpStatus.BAD_REQUEST);
			}else {
				e.setDirection(direction);
			}
		}
		
		serviceRepository.save(e);
		return ResponseEntity.ok(e);
	  }

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Service> getAllCompetences() {
	    return serviceRepository.findAll();
	  }
	  
	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Service e) {
		  Optional<Service> ser = serviceRepository.findById(e.getCodePosition());
			if (ser.isPresent()) {
				Service service = ser.get();
				service.setCodeService(e.getCodeService());
				service.setIntitulePosition(e.getIntitulePosition());
				service.setDirection(e.getDirection());
				serviceRepository.save(service);
				return ResponseEntity.ok(e);

			}else {
				return new ResponseEntity<>(new ResponseMessage("Fail -> service n'existe pas!"),
						HttpStatus.BAD_REQUEST);
			}
	  }
	  
	  @DeleteMapping("delete/{id}")
	  public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	  {
		  Optional<Service> ser = serviceRepository.findById(id);
		  if (ser.isPresent()) {
			  if (objectifRepository.findByPosition(ser.get().getCodePosition()).isEmpty()) {
				  if (posteRepository.findByPosition(ser.get()).isEmpty()) {
					  serviceRepository.delete(ser.get());
					  return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
								HttpStatus.OK);
				  }
				  return new ResponseEntity<>(new ResponseMessage("Erreur -> Des postes ont deja cette position, il faut les changer avant de le supprimé!"),
							HttpStatus.FORBIDDEN);
				  
			  }
			  return new ResponseEntity<>(new ResponseMessage("Erreur -> Des objectifs ont deja cette position, il faut les changer avant de le supprimé!"),
						HttpStatus.FORBIDDEN);
	            
	        }
	  
		  return new ResponseEntity<>(new ResponseMessage("Fail -> Service n'existe pas!"),
					HttpStatus.NOT_FOUND);
	  }
}
