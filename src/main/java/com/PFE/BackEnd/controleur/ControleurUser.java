package com.PFE.BackEnd.controleur;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Role;
import com.PFE.BackEnd.Model.User;
import com.PFE.BackEnd.Security.jwt.JwtProvider;
import com.PFE.BackEnd.Security.jwt.message.request.LoginForm;
import com.PFE.BackEnd.Security.jwt.message.request.SignUpForm;
import com.PFE.BackEnd.Security.jwt.message.response.JwtResponse;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.Security.services.UserPrinciple;
import com.PFE.BackEnd.mail.EmailServiceImpl;
import com.PFE.BackEnd.repository.RoleRepository;
import com.PFE.BackEnd.repository.UserRepository;
import com.PFE.BackEnd.service.UserService;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurUser {
	
	@Autowired
	private EmailServiceImpl emailServiceImpl;
	
	@Autowired
    UserService userService; 
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/api/auth/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		
		UserPrinciple userDetails = (UserPrinciple) authentication.getPrincipal();
		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());
		
		return ResponseEntity.ok(new JwtResponse(jwt, 
				 userDetails.getId(), 
				 userDetails.getUsername(), 
				 userDetails.getEmail(), 
				 roles));
	}
	
	
	@PostMapping("/api/auth/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
		/*if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}*/

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

	    String username = userService.generateRandomString();    
	    String password = userService.generateRandomString();
    
	    
		// Creating user's account
		User user = new User(username, signUpRequest.getEmail(),
				encoder.encode(password));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();
		System.out.println(strRoles);
		if (strRoles == null) {
			Role userRole = roleRepository.findByRole("ROLE_USER");
			roles.add(userRole);
		} else {	
		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByRole("ADMIN");
						
				roles.add(adminRole);

				break;
				
			case "mod":
				Role modRole = roleRepository.findByRole("ROLE_MODERATOR");
				roles.add(modRole);

				break;
				
			default:
				Role userRole = roleRepository.findByRole("ROLE_USER");
				roles.add(userRole);
			}
		});
		}
		user.setRoles(roles);
		userRepository.save(user);
		emailServiceImpl.sendSimpleMessage(user.getEmail(), "Username and Password", "Username : " + username + "\n Password : " + password + "\nHadou rahom auto generated btw");
		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}
	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}
	
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public String userAccess() {
		return "User Content.";
	}

	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}
	
	//------------------- Update a User --------------------------------------------------------
    
    @PutMapping(value = "/user/updateInfo")
    public  ResponseEntity<?> updateUser(@RequestBody User user) {
       
        Optional<User> currentUser = userRepository.findById(user.getId());
          
        if (currentUser.isPresent()) {
        	
        	User upuser  = currentUser.get();
        	if (!user.getUsername().equals(upuser.getUsername())) {
        		
        		if (!upuser.getUsername().equals(user.getUsername()) && userRepository.existsByUsername(user.getUsername())) {
    				return new ResponseEntity<>(new ResponseMessage("Erreur -> Username deja utilisé!"),
    						HttpStatus.NOT_ACCEPTABLE);
    			}else {
    				upuser.setUserName(user.getUsername());
    			}
        	}
        	
        	if (!user.getEmail().equals(upuser.getEmail())) {
        		
        		if (!upuser.getEmail().equals(user.getEmail()) && userRepository.existsByEmail(user.getEmail())) {
    				return new ResponseEntity<>(new ResponseMessage("Erreur -> Email deja utilisé!"),
    						HttpStatus.NOT_ACCEPTABLE);
    			}else {
    				upuser.setEmail(user.getEmail());
    			}
        	}
			        	
        	userRepository.save(upuser);
            return new ResponseEntity<User>(HttpStatus.OK);
            
        }
  
        System.out.println("User with id " + user.getId() + " not found");
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        
    }
    
    @PutMapping(value = "/user/updatePass")
    public ResponseEntity<User> updateUserPassword( @RequestBody User user) {
       
        Optional<User> currentUser = userRepository.findById(user.getId());
          
        if (currentUser.isPresent()) {
        	User upuser  = currentUser.get();
        	
        	upuser.setPassword(encoder.encode(user.getPassword()));
        	userRepository.save(upuser);
        	emailServiceImpl.sendSimpleMessage(upuser.getEmail(), "Update Password", "Hi " + upuser.getUsername() + "\n your Password is updated");
            return new ResponseEntity<User>(HttpStatus.OK);
            
        }
  
        System.out.println("User with id " + user.getId() + " not found");
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        
    }
    
    
    @PostMapping("/user/checkPassword")
	public ResponseEntity<?> checkPassword(@RequestBody User u) {

    	Optional<User> user = userRepository.findById(u.getId());
        if (user.isPresent()) {
        	return ResponseEntity.ok(userService.matches(u.getPassword(), user.get().getPassword()));
		}
        return new ResponseEntity<>(new ResponseMessage("Erreur -> user n'existe pas!"),
				HttpStatus.NOT_FOUND);
		
	}
    
    @PostMapping("/user/forgotPassword")
	public ResponseEntity<?> forgotPassword(@RequestBody User user) {

		User u = userService.findUserByEmail(user.getEmail());
		if (u == null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> email n'existe pas!"),
					HttpStatus.NOT_FOUND);
		}
		String password = userService.generateRandomString();
		u.setPassword(password);
		userRepository.save(user);
		emailServiceImpl.sendSimpleMessage(u.getEmail(), "Reset Password", "Hi " + u.getUsername() + "\nTon nouveau mot de passe est:" + password);
        return new ResponseEntity<User>(HttpStatus.OK);
	}
  
    //------------------- Delete a User --------------------------------------------------------
      
    @DeleteMapping(value = "/user/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long id) {
        System.out.println("Fetching & Deleting User with id " + id);
  
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
        	userRepository.deleteById(id);
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
            
        }
  
        System.out.println("Unable to delete. User with id " + id + " not found");
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
    }
    
    
    @GetMapping(value = "/email/{id}")
    public ResponseEntity<?> SendUserEmail(@PathVariable("id") Long id) {
        
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
        	try {
        		emailServiceImpl.sendSimpleMessage(user.get().getEmail(), "message de test de stmp mail", 
        				"Hi " + user.get().getUsername() + "\nThis is just a test, to make sure it's working");
        		return new ResponseEntity<>(new ResponseMessage("Email Envoyé!"),
						HttpStatus.OK);
        	}catch (Exception e) {
        		e.printStackTrace();
        	}
        	
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> User n'existe pas!"),
				HttpStatus.BAD_REQUEST);
    }
    
    @GetMapping(value = "/findByEmail/{email}")
    public ResponseEntity<?> findByEmail(@PathVariable("email") String email) {
        
        User user = userRepository.findByEmail(email);
        if (user != null) {
        	try {
        		return new ResponseEntity<>(user,
						HttpStatus.OK);
        	}catch (Exception e) {
        		e.printStackTrace();
        	}
        	
            
        }
  
        return new ResponseEntity<>(new ResponseMessage("Erreur -> User n'existe pas!"),
				HttpStatus.BAD_REQUEST);
    }
/*	
	@RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }


    @RequestMapping(value="/registration", method = RequestMethod.GET)
    public ModelAndView registration(){
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findUserByUserName(user.getUserName());
        if (userExists != null) {
            bindingResult
                    .rejectValue("userName", "error.user",
                            "There is already a user registered with the user name provided");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
        } else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been registered successfully");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("registration");

        }
        return modelAndView;
    }
    
    @PostMapping(path = "/login")
    public @ResponseBody boolean loginUser(@RequestBody User user) {
        User userExists = userService.findUserByUserName(user.getUserName());
        if (userExists != null) {
            return false;
        }else {
            return true;

        }
    }

    @RequestMapping(value="/admin/home", method = RequestMethod.GET)
    public ModelAndView home(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        modelAndView.addObject("userName", "Welcome " + user.getUserName() + "/" + " (" + user.getEmail() + ")");
        modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }

      
    //-------------------Retrieve All Users--------------------------------------------------------
      
	@PostMapping(path = "/login")
    public @ResponseBody boolean loginUser(@RequestBody User user) {
        User userExists = userService.findUserByUserName(user.getUserName());
        if (userExists != null) {
            return true;
        }else {
            return false;

        }
    }
	
    @GetMapping(value = "/user/")
    public@ResponseBody Iterable<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        Iterable<User> it = userService.findAllUsers();
        it.forEach(users::add);
        if(users.isEmpty()){
            return userService.findAllUsers();
        }
        return userService.findAllUsers();
    }
  
  
    //-------------------Retrieve Single User--------------------------------------------------------
      
    @GetMapping(value = "/user/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
        System.out.println("Fetching User with id " + id);
        Optional<User> user = userService.findById(id);
        if (user.isPresent()) {
        	return new ResponseEntity<User>(user.get(), HttpStatus.OK);
        }
        
        System.out.println("User with id " + id + " not found");
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
    }
  
      
      
    //-------------------Create a User--------------------------------------------------------
      
    @PostMapping(value = "/user/")
    public ResponseEntity<Void> createUser(@RequestBody User user) {
        System.out.println("Creating User " + user.getUserName());
  
        if (userService.findByUserName(user.getUserName()) != null) {
            System.out.println("A User with name " + user.getUserName() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        userService.save(user);
  
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
  
      
    //------------------- Update a User --------------------------------------------------------
      
    @PutMapping(value = "/user/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        System.out.println("Updating User " + id);
          
        Optional<User> currentUser = userService.findById(id);
          
        if (currentUser.isPresent()) {
        	user.setId(id);
            userService.save(user);
            return new ResponseEntity<User>(HttpStatus.OK);
            
        }
  
        System.out.println("User with id " + id + " not found");
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        
    }
  
    //------------------- Delete a User --------------------------------------------------------
      
    @DeleteMapping(value = "/user/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long id) {
        System.out.println("Fetching & Deleting User with id " + id);
  
        Optional<User> user = userService.findById(id);
        if (user.isPresent()) {
        	userService.deleteUserById(id);
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
            
        }
  
        System.out.println("Unable to delete. User with id " + id + " not found");
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
    }
  
      
    //------------------- Delete All Users --------------------------------------------------------
      
   */ 
  
}