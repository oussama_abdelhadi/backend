package com.PFE.BackEnd.controleur;


import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Employe;
import com.PFE.BackEnd.Model.Role;
import com.PFE.BackEnd.Model.User;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.mail.EmailServiceImpl;
import com.PFE.BackEnd.repository.EmployeRepository;
import com.PFE.BackEnd.repository.RoleRepository;
import com.PFE.BackEnd.repository.UserRepository;
import com.PFE.BackEnd.service.EmployeService;
import com.PFE.BackEnd.service.UserService;

@RequestMapping("emp")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurEmploye {
	
	private EmployeService employeService;
	
	@Autowired
	private EmailServiceImpl emailServiceImpl;
	@Autowired
    UserService userService; 
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EmployeRepository employeRepository;
	
	@Autowired
	RoleRepository roleRepository;


	
	@Autowired
	public ControleurEmploye(EmployeService employeService) {
		this.employeService=employeService;
	}
	
	
	@PostMapping(path="/add")
	public ResponseEntity<?> addNewEmploye (@RequestBody Employe e) {
		if (e.getUser()!=null) {
			
			/*if (userRepository.existsByUsername(e.getUser().getUsername()) && userRepository.existsByEmail(e.getUser().getEmail())) {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Username et Email deja utilisé!"),
						HttpStatus.NOT_ACCEPTABLE);
			}
			
			if (userRepository.existsByUsername(e.getUser().getUsername())) {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Username deja utilisé!"),
						HttpStatus.NOT_ACCEPTABLE);
			}*/
			
			if (userRepository.existsByEmail(e.getUser().getEmail())) {
				return new ResponseEntity<>(new ResponseMessage("Erreur -> Email deja utilisé!"),
						HttpStatus.NOT_ACCEPTABLE);
			}
			
			String username = userService.generateRandomString();    
		    String password = userService.generateRandomString();
		    
			User user = new User(username, e.getUser().getEmail(),
					new BCryptPasswordEncoder().encode(password));

			Set<Role>roles = new HashSet<>();
			Role userRole = roleRepository.findByRole("ROLE_USER");
			roles.add(userRole);
				
			user.setRoles(roles);
			userRepository.save(user);
			User u = userRepository.findByUsername(user.getUsername());
			e.setUser(u);
			
			employeService.save(e);
			
			
			emailServiceImpl.sendSimpleMessage(user.getEmail(), "Username and Password", "Username : " + username + "\n Password : " + password);
			return ResponseEntity.ok(e);
		}
		
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> User n'existe pas dans le JSON envoyé!"),
				HttpStatus.BAD_REQUEST);
		
	}

	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Employe> getAllEmployes() {
	    return employeService.findAll();
	  }
	  
	  @GetMapping(path="/getEmploye/{id}")
	  public ResponseEntity<?> getEmployeUser(@PathVariable("id") Long id) {
		  Optional<User> usr = userRepository.findById(id);
		  if (usr.isPresent()) {
			  User current = usr.get();
			  Optional<Employe> emp = employeRepository.findByUser(current);
			  if (emp.isPresent()) {
				  return ResponseEntity.ok(emp.get());
			  }
			  return new ResponseEntity<>(new ResponseMessage("Erreur -> Pas d'employe avec cet User!"),
						HttpStatus.BAD_REQUEST);
	        }
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Aucun User avec ce ID!"),
					HttpStatus.BAD_REQUEST);
	  }
	  
	  @PostMapping(path="/collabs")
	  public ResponseEntity<List<Employe>> getAllCollabs(@RequestBody Employe e) {
	    
		  Optional<Employe> emp = employeService.findById(e.getCodeEmploye());
          
	        if (emp.isPresent()) {
	        	Employe sup = emp.get();
	        	List<Employe> lsEmp = employeService.findBySupperieur(sup);
	            return new ResponseEntity<List<Employe>>(lsEmp,HttpStatus.OK);
	            
	        }
	  
	        System.out.println("l'employé n'existe pas ou il n'est pas un superieur ");
	        return new ResponseEntity<List<Employe>>(HttpStatus.NOT_FOUND);
	  
	  }

	  @PutMapping(path="/update")
	  public ResponseEntity<?> update (@RequestBody Employe e) {
		  Optional<Employe> emp = employeService.findById(e.getCodeEmploye());
          
	        if (emp.isPresent()) {
	        	Employe employe = emp.get();
	        	employe.setNom(e.getNom());
	        	employe.setPrenom(e.getPrenom());
	        	employe.setSuperieur(e.getSuperieur());
	        	employe.setPoste(e.getPoste());
	        	employeService.save(employe);
				return ResponseEntity.ok(e);
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> Employe Doesn't Exist!"),
					HttpStatus.NOT_FOUND);
	  }
	  
	  @PutMapping(path="/passCollab/{id}")
	  public @ResponseBody ResponseEntity<Employe> passCollab (@PathVariable("id") Integer id,@RequestBody Employe e) {
		  	Optional<Employe> emp = employeService.findById(e.getCodeEmploye());
          
	        if (emp.isPresent()) {
	        	Optional<Employe> newEmp = employeService.findById(id);
	        	if (newEmp.isPresent()) {
	        	
	        		Employe sup = emp.get();
	        		Employe newSup = newEmp.get();
		        	List<Employe> lsEmp = employeService.findBySupperieur(sup);
		        	
		        	for (Employe col : lsEmp) {
		        		col.setSuperieur(newSup);
		        		employeService.save(col);
		        	}
		            return new ResponseEntity<Employe>(HttpStatus.OK);
	        	}
	        	
	        	System.out.println("Employé with id " + id + " not found");
	        	return new ResponseEntity<Employe>(HttpStatus.NOT_FOUND);
	            
	        }
	  
	        System.out.println("Employé with id " + id + " not found");
	        return new ResponseEntity<Employe>(e,HttpStatus.NOT_FOUND);
	  }
	  
	  @DeleteMapping("delete/{id}")
	  public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	  {
		  Optional<Employe> emp = employeRepository.findById(id);
		  if (emp.isPresent()) {
	        	
			  employeRepository.delete(emp.get());
			  return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
						HttpStatus.OK);
	            
	        }
	  
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Employe n'existe pas!"),
					HttpStatus.NOT_FOUND);
	  }
	  
	  @GetMapping(path="/getSup/{id}")
	  public ResponseEntity<?> getEmployePossibleSups(@PathVariable("id") Integer id) {
		  Optional<Employe> emp = employeRepository.findById(id);
		  if (emp.isPresent()) {
			  
			  return ResponseEntity.ok(employeService.findPotentielSupperieur(emp.get()));
		  }
		  return new ResponseEntity<>(new ResponseMessage("Erreur -> Pas d'employe avec cet User!"),
					HttpStatus.BAD_REQUEST);
	  }
	
}
