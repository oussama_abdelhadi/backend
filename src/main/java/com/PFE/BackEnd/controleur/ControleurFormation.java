package com.PFE.BackEnd.controleur;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.PFE.BackEnd.Model.Formation;
import com.PFE.BackEnd.Model.Phase;
import com.PFE.BackEnd.Security.jwt.message.response.ResponseMessage;
import com.PFE.BackEnd.repository.EmployeRepository;
import com.PFE.BackEnd.repository.FormationRepository;
import com.PFE.BackEnd.repository.PhaseRepository;

@RequestMapping("Formation")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ControleurFormation {

	

	@Autowired
	private FormationRepository formationRepository;
	
	@Autowired
	private PhaseRepository phaseRepository;
	
	@Autowired
	private EmployeRepository employeRepository;
	
	
	@PostMapping(path="/add")
	  public ResponseEntity<?> addNewFormation (@RequestBody Formation f) {
		
		if ((f.getNomFormation()==null)||(f.getNomFormation()=="")) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Imposible de cree une formation sans nom!"),
					HttpStatus.NOT_ACCEPTABLE);
		}
		if (f.getEmploye() == null) {
			return new ResponseEntity<>(new ResponseMessage("Erreur -> Imposible de cree une formation sans employe!!"),
					HttpStatus.NOT_ACCEPTABLE);
		}
		if (f.getDateFormation() == null) {
			f.setDateFormation(phaseRepository.findAll().get(0).getDate());
		}
		formationRepository.save(f);
		return ResponseEntity.ok(f);
	  }
	
	

	  @GetMapping(path="/all/{id}")
	  public ResponseEntity<?> getAllFormations(@PathVariable Integer id) {
		  
		  if (employeRepository.findById(id).isPresent()) {
			  Phase phase = phaseRepository.findAll().get(0);
			  ArrayList<Formation> formList = (ArrayList<Formation>) formationRepository.findByEmploye(employeRepository.findById(id).get());
			  ArrayList<Formation> newList = new ArrayList<Formation>();
			  for (Formation form : formList) {
				  if (form.getDateFormation().equals(phase.getDate())) {
					  newList.add(form);
				  }
			  }
			  return ResponseEntity.ok(newList);
		  }
		
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
		  
	  }
	  
	  @GetMapping(path="/lastYear/{id}")
	  public ResponseEntity<?> getLastYearFormations(@PathVariable Integer id) {
		  
		  if (employeRepository.findById(id).isPresent()) {
			  int i = phaseRepository.findAll().get(0).getDate() - 1 ;
			  ArrayList<Formation> formList = (ArrayList<Formation>) formationRepository.findByEmploye(employeRepository.findById(id).get());
			  ArrayList<Formation> newList = new ArrayList<Formation>();
			  for (Formation form : formList) {
				  if (form.getDateFormation().equals(i)) {
					  newList.add(form);
				  }
			  }
			  return ResponseEntity.ok(newList);
		  }
		
		
		return new ResponseEntity<>(new ResponseMessage("Erreur -> Un employe avec ce ID n'existe pas!"),
				HttpStatus.BAD_REQUEST);
		  
	  }
	  
	  @PutMapping(path="/update")
		public ResponseEntity<?> update (@RequestBody Formation f) {
			Optional<Formation> p = formationRepository.findById(f.getCodeFormation());
	        
	        if (p.isPresent()) {
	        	Formation formation = p.get();
	        	formation.setDateFormation(f.getDateFormation());
	        	formation.setNomFormation(f.getNomFormation());
	        	formation.setObjectifPrevu(f.getObjectifPrevu());
	        	formation.setEvalColl(f.getEvalColl());
	        	formation.setEvalResp(f.getEvalResp());
	        	formation.setJustification(f.getJustification());
	        	formation.setDemande(f.getDemande());
	        	formationRepository.save(formation);
				return ResponseEntity.ok(f);
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> La formation n'existe pas!"),
					HttpStatus.NOT_FOUND);
		}
	  
	  @DeleteMapping("delete/{id}")
	    public ResponseEntity<?> delete(@PathVariable("id") Integer id)
	    {
			Optional<Formation> f = formationRepository.findById(id);
			if (f.isPresent()) {
	        	try {
	        		Formation formation = f.get();
	        		formationRepository.delete(formation);
	    			return new ResponseEntity<>(new ResponseMessage("Supprimé!"),
	    					HttpStatus.OK);
	        	}catch(Exception e) {
	        		return new ResponseEntity<>(new ResponseMessage("" + e.getStackTrace()),
	        				HttpStatus.FORBIDDEN);
	        	}
				
	            
	        }
	  
	        return new ResponseEntity<>(new ResponseMessage("Erreur -> la formation n'existe pas!"),
					HttpStatus.NOT_FOUND);
	    }
}
