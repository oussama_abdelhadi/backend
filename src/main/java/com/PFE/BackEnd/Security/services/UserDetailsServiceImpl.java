package com.PFE.BackEnd.Security.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.PFE.BackEnd.Model.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	com.PFE.BackEnd.repository.UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			User user = userRepository.findByUsername(username);
			return UserPrinciple.build(user);
		}catch (UsernameNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		
	}
}