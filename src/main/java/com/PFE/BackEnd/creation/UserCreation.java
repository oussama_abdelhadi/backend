package com.PFE.BackEnd.creation;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.PFE.BackEnd.Model.Phase;
import com.PFE.BackEnd.Model.Role;
import com.PFE.BackEnd.Model.User;
import com.PFE.BackEnd.repository.PhaseRepository;
import com.PFE.BackEnd.repository.RoleRepository;
import com.PFE.BackEnd.repository.UserRepository;

@Component
public class UserCreation {

	@Autowired
    private UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	private PhaseRepository phaseRepository;
	
    @PostConstruct
    public void init() {
    	try {
    	if (!userRepository.existsByUsername("Admin")) {
    		
    		Set<Role> roles = new HashSet<>();
        	
    		
        	Role adminRole = new Role(1,"ADMIN");
        	System.out.println(adminRole);
    		roles.add(adminRole);
    		
    		User user = new User("Admin","Admin@test.test", new BCryptPasswordEncoder().encode("Admin"));
    		System.out.println(user + "\n" + roles);
    		user.setRoles(roles);
            userRepository.save(user);
    	}
    	if (phaseRepository.findAll().isEmpty()) {
    		Phase phase = new Phase (-1, -1);
    		phaseRepository.save(phase);
    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
